class AuthModel {
  int? id;
  String? nama;
  String? email;
  String? tglLahir;
  String? noTelpon;
  String? token;
  
  AuthModel({this.id, this.nama, this.email, this.tglLahir, this.noTelpon, this.token});
}