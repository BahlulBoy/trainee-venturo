// ignore_for_file: list_remove_unrelated_type

import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/modules/features/checkout/repositories/checkout_repository.dart';
import 'package:trainee/modules/features/checkout/views/components/finger_print_dialog.dart';
import 'package:trainee/modules/features/checkout/views/components/order_success_dialog.dart';
import 'package:trainee/modules/features/checkout/views/components/pin_dialog.dart';

import '../views/components/order_fail_dialog.dart';

class CheckoutController extends GetxController{
  static CheckoutController get to => Get.find<CheckoutController>();

  RxList<Map<String, dynamic>> cart = <Map<String, dynamic>>[].obs;
  RxList<Map<String, dynamic>> diskonList = <Map<String, dynamic>>[].obs;
  RxMap<String, dynamic> voucher = <String, dynamic>{}.obs;
  RxString cartViewState = 'success'.obs;
  RxInt diskon = 0.obs;

  @override
  void onInit() async {
    super.onInit();
    cart.value = Get.arguments;
    var idUser = await Hive.box('venturo').get('id');
    var token = await Hive.box('venturo').get('token');
    var resultCheckDiskon = await CheckoutRepository.getDiskon(idUser, token);
    if (resultCheckDiskon.data.containsKey('data')) {
      diskonList.addAll(resultCheckDiskon.data['data']);
      for (var element in diskonList) { 
        var i = element['diskon'] as int;
        diskon += i;
      }
    } else {
      log(resultCheckDiskon.data.toString());
    }
    log(idUser.toString());
  }

  void increaseJumlah(Map<String, dynamic> item) {
    item['jumlah']++;
    cart.refresh();
  }

  void decreaseJumlah(Map<String, dynamic> item) {
    if (item['jumlah'] > 1) {
      item['jumlah']--;
      cart.refresh();
    } else if(item['jumlah'] == 1) {
      var indexI = cart.indexWhere((element) => element['id_menu'] == item['id_menu']);
      cart.removeAt(indexI);
    }
  }

  List<Map<String, dynamic>> get foodItems =>
    cart.where((e) => e['item']['kategori'] == 'makanan').toList();

  /// Get drink items
  List<Map<String, dynamic>> get drinkItems => 
    cart.where((e) => e['item']['kategori'] == 'minuman').toList();

  List<Map<String, dynamic>> get snackItems =>
    cart.where((e) => e['item']['kategori'] == 'snack').toList();

  // calculate total price of all item
  int get totalPrice => cart.fold(
    0,
    (prevTotal, item) =>
        prevTotal + (item['item']['harga'] as int) * item['jumlah'] as int);

  // calculate discount price
  int get discountPrice => totalPrice ~/ (100/diskon.value);

  int get vourcherPrice {
    if (voucher.containsKey('nominal')) {
      if (voucher['nominal'] is int) {
        log(voucher.toString());
        return voucher['nominal'];
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }

  // calculate final price
  int get grandTotalPrice {
    var result = totalPrice - discountPrice - vourcherPrice;
    if (result > 0) {
      return result;
    } else {
      return 0;
    }
  }

  void getVoucher() async {
    var result = await Get.toNamed(MainRoute.vourcher);
    if (result.isNotEmpty && result != null) {
      voucher.value = result;
    } else {
      voucher.value = {};
    }
  }

  /// menuju ke halaman detail lalu kembali lagi dengan data yang terbaru
  void getDetail(Map<String, dynamic> item) async {
    var indexCheckData = cart.indexWhere(
      (element) => element['id_menu'] == item['id_menu']
    );

    if (indexCheckData != -1) {
      var argument = cart[indexCheckData];
      var data = await Get.toNamed(MainRoute.detailMenu, arguments: argument);
      if (data != null && data.containsKey('id_menu')) {
        log(data.toString());
        if (data['jumlah'] != 0) {
          cart[indexCheckData] = data;
        } else {
          cart.remove(indexCheckData);
        }
      }
    }
  }

  Future<void> verify() async{
    // check supported auth type in device
    final LocalAuthentication localAuth = LocalAuthentication();
    final bool canCheckBiometrics = await localAuth.canCheckBiometrics;
    final bool isBiometricSupported = await localAuth.isDeviceSupported();

    if (canCheckBiometrics && isBiometricSupported) {
      // open fingerprint dialog if supported
      final String? authType = await showFingerprintDialog();

      if (authType == 'fingerprint') {
        // fingerprint auth flow
        final bool authenticated = await localAuth.authenticate(
          localizedReason: 'Please authenticate to confirm order'.tr,
          options: const AuthenticationOptions(
            biometricOnly: true,
          ),
        );


        // if succeed, order cart
        if (authenticated) {
          showOrderSuccessDialog();
        }
      } else if (authType == 'pin') {
        // pin auth flow
        await showPinDialog();
      }
    } else {
      await showPinDialog();
    }
  }

  Future<String?> showFingerprintDialog() async {
    // ensure all modal is closed before show fingerprint dialog
    Get.until(ModalRoute.withName(MainRoute.checkout));
    final result = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const FingerPrintDialog(),
    );
    return result;
  }
  
  /// Memunculkan pin dialog
  Future<void> showPinDialog() async {
    // ensure all modal is closed before show pin dialog
    Get.until(ModalRoute.withName(MainRoute.checkout));


    const userPin = '123456';


    final bool? authenticated = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const PinDialog(pin: userPin),
    );


    if (authenticated == true) {
      // if succeed, order cart
      showOrderSuccessDialog();
    } else if (authenticated != null) {
      // if failed 3 times, show order failed dialog
      Get.until(ModalRoute.withName(MainRoute.checkout));
      Get.showSnackbar(const GetSnackBar(
        title: 'Error',
        message: 'PIN already wrong 3 times. Please try again later.',
      ));
    }
  }

  Future<void> showNotif() async {
    const androidNotifDetail = AndroidNotificationDetails(
        'order_notification_channel', 'order channel',
        channelDescription: 'order data channel',
        priority: Priority.high);

    const NotificationDetails notifDetail = NotificationDetails(
      android: androidNotifDetail,
    );

    FlutterLocalNotificationsPlugin().show(
      1,
      'Order',
      'Order Telah dibuat',
      notifDetail,
    );
  }
  
  /// melakukan submitOrderApi() lalu jika berhasil maka akan memunculkan OrderSuccessDialog dan kembali ke halaman sebelumnya
  Future<void> showOrderSuccessDialog() async {
    Get.until(ModalRoute.withName(MainRoute.checkout));
    var result = await submitOrderApi();
    log(result['status_code'].toString());
    if (result['status_code'] == 200) {
      await Get.defaultDialog(
        title: '',
        titleStyle: const TextStyle(fontSize: 0),
        content: const OrderSuccessDialog(),
      );
      showNotif();
      Get.back(result: 'success');
    } else {
      await Get.defaultDialog(
        title: '',
        titleStyle: const TextStyle(fontSize: 0),
        content: const OrderFailDialog(),
      );
    }
  }

  /// Mengirimkan data order ke api
  Future<dynamic> submitOrderApi() async {
    var token = await Hive.box('venturo').get('token');
    var idUser = await Hive.box('venturo').get('id');
    var idVoucher = 0;
    var diskon = 0;
    var potongan = 0;
    var total = grandTotalPrice;
    var item = [];
    Map<String, dynamic> body = {};
    body['order'] = {
      'id_user' : idUser,
      'total_bayar' : total
    };
    for (var element in cart) {
      var result = {};
      result['id_menu'] = element['id_menu'];
      result['harga'] = element['harga'];
      result['jumlah'] = element['jumlah'];
      if (element.containsKey('catatan')) {
        result['catatan'] = element['catatan'];
      }
      if (element.containsKey('level')) {
        result['level'] = element['level']['id_detail'];
      }
      if (element.containsKey('toping') && element['toping'] is List) {
        result['toping'] = [];
        element['toping'].forEach((e) {
          result['toping'].add(e['id_detail']);
        });
      }
      item.add(result);
    }
    body['menu'] = item;
    if (diskonList.isNotEmpty) {
      body['order']['diskon'] = diskon;
      var listDiskon = [];
      for (var element in diskonList) {
        listDiskon.add(element['id_diskon']);
      }
      potongan = discountPrice;
      body['order']['id_diskon'] = listDiskon;
      body['order']['potongan'] = potongan;
    } else if (voucher.isNotEmpty) {
      idVoucher = voucher['id_voucher'];
      body['order']['id_voucher'] = idVoucher;
      potongan = voucher['nominal'];
      body['order']['potongan'] = potongan;
    }
    var result = await CheckoutRepository.submitOrder(token, body);
    return result;
  }
}