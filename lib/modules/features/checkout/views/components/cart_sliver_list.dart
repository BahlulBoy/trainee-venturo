import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trainee/modules/features/beranda/views/components/menu_card.dart';
import 'package:trainee/modules/features/checkout/controllers/checkout_controller.dart';

class CartListSliver extends StatelessWidget{
  const CartListSliver({
    super.key,
    required this.carts,
  });


  final List<Map<String, dynamic>> carts;

  @override
  Widget build(BuildContext context) {
    return SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 8.5.h),
            child: MenuCard(
              menu: carts[index]['item'],
              onAdd: () => CheckoutController.to.increaseJumlah(carts[index]),
              onRemove: () => CheckoutController.to.decreaseJumlah(carts[index]),
              onTap: () {
                CheckoutController.to.getDetail(carts[index]);
              },
              total: carts[index]['jumlah'] as int
            ),
          );
        },
        childCount: carts.length
      ), 
      itemExtent: 112.h
    );
  }

}