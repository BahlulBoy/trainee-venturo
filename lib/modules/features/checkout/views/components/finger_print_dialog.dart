import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class FingerPrintDialog extends StatelessWidget{
  const FingerPrintDialog({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Verify order'.tr,
            style: Get.textTheme.headlineMedium,
          ),

          Text(
            'Press your fingerprint'.tr,
            style: Get.textTheme.bodySmall!.copyWith(color: Colors.black),
          ),
          30.verticalSpacingRadius,

          GestureDetector(
            child: Icon(Icons.fingerprint,
                size: 80.r, color: Theme.of(context).primaryColor),
            onTap: () => Get.back<String>(result: 'fingerprint'),
          ),
          30.verticalSpacingRadius,

          TextButton(
          onPressed: () => Get.back<String>(result: 'pin'),
          child: Text(
            'Verify using PIN code',
            style: Get.textTheme.titleSmall!
                .copyWith(color: Theme.of(context).primaryColor),
          ),
          ),
        ]
      ),
    );
  }


}