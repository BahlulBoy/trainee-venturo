import 'dart:developer';

import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/beranda/views/components/section_header.dart';
import 'package:trainee/modules/features/checkout/controllers/checkout_controller.dart';
import 'package:trainee/modules/features/checkout/views/components/card_order_bottom_bar.dart';
import 'package:trainee/modules/features/checkout/views/components/cart_sliver_list.dart';
import 'package:trainee/modules/features/checkout/views/components/rounded_app_bar.dart';
import 'package:trainee/modules/features/checkout/views/components/tile_option.dart';

class CheckoutView extends StatelessWidget{
  const CheckoutView({super.key});

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics.instance.setCurrentScreen(
      screenName: 'checkout screen',
      screenClassOverride: 'Trainee'
    );
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: const RoundedAppBar(
        title: 'Pesanan',
        icon: Icons.shopping_cart_checkout,
      ),
      body: Obx(() => CustomScrollView(
        physics: const ClampingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(child: 17.verticalSpace),
          SliverToBoxAdapter(
            child: SectionHeader(
              title: 'Food',
              icon: Icons.food_bank_outlined,
              color: Theme.of(context).primaryColor,
            ),
          ),
          SliverPadding(
            padding:
                EdgeInsets.symmetric(horizontal:25.w, vertical: 3.h),
            sliver: CartListSliver(
              carts: CheckoutController.to.foodItems,
            )
          ),
          SliverToBoxAdapter(child: 17.verticalSpace),
          if (true) ...[
              SliverToBoxAdapter(
                child: SectionHeader(
                  icon: Icons.local_drink_outlined,
                  title: 'Drink',
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Obx(() {
                log(CheckoutController.to.drinkItems.toString());
                return SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 3.h),
                  sliver: CartListSliver(
                    carts: CheckoutController.to.cart.where((e) => e['item']['kategori'] == 'minuman').toList(),
                  ),
                );
              })
          ],
          SliverToBoxAdapter(child: 17.verticalSpace),
          if (true) ...[
            SliverToBoxAdapter(
              child: SectionHeader(
                icon: Icons.food_bank,
                title: 'Snack',
                color: Theme.of(context).primaryColor,
              ),
            ),
            SliverPadding(
              padding:
                  EdgeInsets.symmetric(horizontal: 25.w, vertical: 3.h),
              sliver: CartListSliver(
                carts: CheckoutController.to.snackItems,
              ),
            )
        ]
        ],
      )),
      bottomNavigationBar: Obx(() =>
        Container(
          decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(30.r),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 25.h, horizontal: 22.w),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // Total order tile
                    TileOption(
                      title: 'Total orders',
                      subtitle: '(${CheckoutController.to.cart.length} Menu):',
                      icon: Icons.payments_outlined,
                      message:
                          'Rp ${CheckoutController.to.totalPrice.toString()}',
                      titleStyle: Get.textTheme.bodyLarge,
                      messageStyle: Get.textTheme.labelLarge!
                          .copyWith(color: Theme.of(context).primaryColor),
                    ),
                    Divider(color: Colors.black54, height: 2.h),
                    
                    if (CheckoutController.to.diskon.value == 0)
                      // Discount tile
                      TileOption(
                        icon: Icons.discount_outlined,
                        iconSize: 24.r,
                        title: 'Discount',
                        message: '- Rp ${CheckoutController.to.discountPrice}',
                        titleStyle: Get.textTheme.bodyLarge,
                        messageStyle: Get.textTheme.labelLarge!
                            .copyWith(color: Theme.of(context).colorScheme.error),
                        onTap: () {},
                      ),

                    // Vourcher tile
                    Obx(() {
                      if (CheckoutController.to.voucher.isEmpty) {
                        return TileOption(
                          icon: BootstrapIcons.ticket_perforated,
                          iconSize: 24.r,
                          title: 'Vourcher',
                          message: 'Pilih Vourcher',
                          titleStyle: Get.textTheme.bodyLarge,
                          messageStyle: Get.textTheme.labelMedium,
                          onTap: () {
                            CheckoutController.to.getVoucher();
                          },
                        );
                      } else {
                        return TileOption(
                          icon: BootstrapIcons.ticket_perforated,
                          iconSize: 24.r,
                          title: 'Vourcher',
                          message: '- Rp ${CheckoutController.to.voucher['nominal'].toString()}',
                          messageSubtitle: CheckoutController.to.voucher['nama'],
                          titleStyle: Get.textTheme.bodyLarge,
                          messageStyle: Get.textTheme.labelMedium!.copyWith(
                            color: MainColor.danger
                          ),
                          onTap: () {
                            CheckoutController.to.getVoucher();
                          },
                        );
                      }
                    }),

                    // Payment options tile
                    TileOption(
                      icon: Icons.payment_outlined,
                      iconSize: 24.r,
                      title: 'Payment',
                      message: 'Pay Later',
                      titleStyle: Get.textTheme.bodyLarge,
                      messageStyle: Get.textTheme.labelMedium,
                    ),

                  ],
                ),
              ),
              CartOrderBottomBar(
                totalPrice: 'Rp ${CheckoutController.to.grandTotalPrice}',
                onOrderButtonPressed: () => CheckoutController.to.verify(),
              )
            ]
          ),
        )
    ));   
  }
}