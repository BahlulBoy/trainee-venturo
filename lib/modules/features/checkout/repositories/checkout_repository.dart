import 'package:trainee/utils/services/http_service.dart';

class CheckoutRepository{
  CheckoutRepository._();

  static Future<dynamic> getDiskon(int id, String token) async {
    return await HttpService.dioCall(token: token).get('/diskon/user/$id');
  }

  static Future<dynamic> submitOrder(String token, Map<String, dynamic> data) async {
    var result = await HttpService.dioCall(token: token).post('/order/add',data: data);
    return result.data as Map<String, dynamic>;
  }
}