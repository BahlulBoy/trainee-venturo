import 'package:hive_flutter/hive_flutter.dart';
import 'package:trainee/utils/services/http_service.dart';

class VoucherRepository{
  Future<dynamic> getListVoucher() async {
    var token = await Hive.box('venturo').get('token');
    var result = await HttpService.dioCall(token: token).get('/voucher/all');
    return result.data;
  }
}