import 'package:flutter/material.dart';
import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/checkout/views/components/custom_rounded_app_bar.dart';
import 'package:trainee/modules/features/vourcher/controllers/vourcher_controller.dart';
import 'package:trainee/modules/features/vourcher/views/components/voucher_card.dart';

class VourcherView extends StatelessWidget{
  const VourcherView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MainColor.white,
        appBar: RoundedAppBar(
          title: 'Pilih Vourcher',
          icon: BootstrapIcons.ticket_perforated,
          onBackButtonPressed: () => Get.back(result: {}),
        ),
        body: Container(
          padding: const EdgeInsets.all(20),
          height: 100.sh,
          width: 100.sw,
          child: Obx(() {
            return ListView.builder(
              itemCount: VourcherController.to.listVoucher.length,
              itemBuilder: (context, index) {
                return Obx(() {
                  return VoucherCard(
                    title: VourcherController.to.listVoucher[index]['nama'].toString(),
                    image: VourcherController.to.listVoucher[index]['info_voucher'],
                    isSelected: VourcherController.to.selectedVoucher['id_voucher'] == VourcherController.to.listVoucher[index]['id_voucher'],
                    onClick: () {
                      VourcherController.to.selectVoucher(index);
                    },
                  );
                });
              },
            );
          })
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5), 
                spreadRadius: 5, 
                blurRadius: 7, 
                offset: const Offset(0, 5), 
              ),
            ],
            color: MainColor.white,
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))  
          ),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  5.horizontalSpace,
                  const Icon(BootstrapIcons.check),
                  5.horizontalSpace,
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: 'Penggunaan voucher tidak dapat digabung dengan ',
                            style: Get.textTheme.labelMedium
                          ),
                          TextSpan(
                            text: 'discount employee reward program',
                            style: Get.textTheme.labelMedium!.copyWith(
                              color: MainColor.primary,
                              fontWeight: FontWeight.w700
                            )
                          )
                        ]
                      ),
                    ),
                  )
                ],
              ),
              10.verticalSpace,
              SizedBox(
                height: 50,
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () => VourcherController.to.submit(), 
                  style: ElevatedButton.styleFrom(
                    backgroundColor: MainColor.primary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                      side: const BorderSide(
                        width: 1.5
                      )
                    )
                  ),
                  child: const Text('Oke')
                ),
              )
            ]
          ),
        ),
      )
    );
  }

}