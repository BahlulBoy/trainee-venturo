import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';

class VoucherCard extends StatelessWidget{
  const VoucherCard({Key? key, required this.title, required this.image, required this.isSelected, this.onClick}) : super(key: key);

  final String title;
  final String image;
  final void Function()? onClick;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        minHeight: 200
      ),
      width: 100.sw,
      padding: const EdgeInsets.only(top: 5),
      margin: const EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: MainColor.background,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 7,
            offset: const Offset(0, 0),
          ),
        ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 30,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text(title,
                    overflow: TextOverflow.ellipsis,
                    style: Get.textTheme.titleSmall!.copyWith(
                      fontSize: 13
                    ),
                  )
                ),
                InkWell(
                  onTap: onClick,
                  child: Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      border: Border.all()
                    ),
                    child: isSelected ? const Icon(BootstrapIcons.check, size: 18) : Container(),
                  ),
                )
              ],
            ),
          ),
          10.verticalSpace,
          SizedBox(
            height: 150,
            width: 100.sw,
            child: CachedNetworkImage(
              imageUrl: image,
              fit: BoxFit.fitWidth,
            ),
          )
        ],
      ),
    );
  }
}