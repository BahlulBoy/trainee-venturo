import 'package:get/get.dart';
import 'package:trainee/modules/features/vourcher/controllers/vourcher_controller.dart';

class VourcherBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(VourcherController());
  }
}