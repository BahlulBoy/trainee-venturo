import 'package:get/get.dart';
import 'package:trainee/modules/features/vourcher/repositories/voucher_repository.dart';

class VourcherController extends GetxController{
  static VourcherController get to => Get.find<VourcherController>();

  final RxList listVoucher = [].obs;
  final RxMap<String, dynamic> selectedVoucher = <String, dynamic>{}.obs;

  @override
  void onInit() async {
    super.onInit();
    var result = await VoucherRepository().getListVoucher();
    if (result['status_code'] == 200) {
      List<dynamic> data = result['data'];
      listVoucher.addAll(data);
    }
  }

  /// memilih vourcher
  void selectVoucher(int index) {
    if (selectedVoucher.containsKey('id_voucher')) {
      if (selectedVoucher['id_voucher'] == listVoucher[index]['id_voucher']) {
        selectedVoucher.value = {};
      } else {
        selectedVoucher.value = listVoucher[index];
      }
    } else {
      selectedVoucher.value = listVoucher[index];
    }
  }

  /// Kembali ke Halaman Sebelumnya
  void submit() {
    Get.back(result: selectedVoucher);
  }
}