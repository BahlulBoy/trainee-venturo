// ignore: depend_on_referenced_packages
import 'package:hive/hive.dart';
import 'package:trainee/modules/features/beranda/modules/order_model.dart';
import 'package:trainee/utils/services/http_service.dart';

class OrderRepository {
  List<Map<String, dynamic>> ongoingOrder = [
    {
      "id_order": 35,
      "no_struk": "001/KWT/01/2022",
      "nama": "dev noersy",
      "total_bayar": 12000,
      "tanggal": "2023-06-19",
      "status": 2,
      "menu": [
        {
          "id_menu": 9,
          "kategori": "makanan",
          "nama": "Nasi Goreng",
          "foto": "https://i.ibb.co/mRJnq3Z/nasi-goreng.jpg",
          "jumlah": 1,
          "harga": "10000",
          "total": 10000,
          "catatan": "test"
        }
      ]
    },
    {
      "id_order": 37,
      "no_struk": "002/KWT/01/2022",
      "nama": "dev noersy",
      "total_bayar": 46000,
      "tanggal": "2023-06-18",
      "status": 3,
      "menu": [
        {
          "id_menu": 3,
          "kategori": "minuman",
          "nama": "Lemon Tea",
          "foto": "https://i.ibb.co/RNXcV2s/chicken-katsu.jpg",
          "jumlah": 2,
          "harga": "18000",
          "total": 36000,
          "catatan": "Testing"
        },
        {
          "id_menu": 9,
          "kategori": "makanan",
          "nama": "Nasi Goreng",
          "foto": "https://i.ibb.co/mRJnq3Z/nasi-goreng.jpg",
          "jumlah": 1,
          "harga": "10000",
          "total": 10000,
          "catatan": ""
        }
      ]
    },
    {
      "id_order": 38,
      "no_struk": "003/KWT/01/2022",
      "nama": "dev noersy",
      "total_bayar": 8100,
      "tanggal": "2023-06-19",
      "status": 1,
      "menu": [
        {
          "id_menu": 9,
          "kategori": "makanan",
          "nama": "Nasi Goreng",
          "foto": "https://i.ibb.co/mRJnq3Z/nasi-goreng.jpg",
          "jumlah": 1,
          "harga": "9000",
          "total": 9000,
          "catatan": "Testing"
        }
      ]
    },
    {
      "id_order": 40,
      "no_struk": "005/KWT/01/2022",
      "nama": "dev noersy",
      "total_bayar": 8100,
      "tanggal": "2023-05-01",
      "status": 4,
      "menu": [
        {
          "id_menu": 9,
          "kategori": "makanan",
          "nama": "Nasi Goreng",
          "foto": "https://i.ibb.co/mRJnq3Z/nasi-goreng.jpg",
          "jumlah": 1,
          "harga": "9000",
          "total": 9000,
          "catatan": "Testing"
        }
      ]
    }
  ];

  Future<OrderResult> onGoingOrderReal() async {
    var token = await Hive.box('venturo').get('token');
    var id = await Hive.box('venturo').get('id');

    var result = await HttpService.dioCall(token: token).get('/order/user/${id.toString()}');
    if (result.statusCode == 200) {
      return OrderResult.fromJson(result.data);
    } else {
      return OrderResult();
    }
  }

  // Get Ongoing Order
  Future<List<DataOrder>> getOngoingOrder() async {
    OrderResult result = await onGoingOrderReal();
    return result.data!.where((element) => element.status! < 3).toList();
  }

  // Get Order History
  Future<List<DataOrder>> getOrderHistory() async {
    OrderResult result = await onGoingOrderReal();
    return result.data!.where((element) => element.status! > 2).toList();
  }

  // Get Order Detail
  Map<String, dynamic>? getOrderDetail(int idOrder) {
    return ongoingOrder.firstWhere((element) => element['id_order'] == idOrder,
        orElse: () => {});
  }
}