import 'dart:developer';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:trainee/utils/services/http_service.dart';

class ListRepository{
  final List<String> imageUrlError = [
    "https://javacode.landa.id/img/menu/gambar_625d150229656.png",
    "https://javacode.landa.id/img/menu/gambar_625d153f59b19.png",
    "https://javacode.landa.id/img/menu/gambar_625d1b75a4077.png",
    "https://javacode.landa.id/img/menu/gambar_625d1b45adee8.png"
  ];

  Future<List<dynamic>> getData() async {
    var token = await Hive.box('venturo').get('token');
    var result = await HttpService.dioCall(token: token).get('/menu/all');
    if (result.statusCode == 200) {
      return result.data['data'];
    } else {
      return [];
    }
  }

  Future<Map<String, dynamic>> getUserDetail({required int id, required String token}) async {
    log(id.toString());
    var result = await HttpService.dioCall(token: token).get('/user/detail/$id');
    if (result.statusCode == 200) {
      final Map<String, dynamic> resultData = result.data['data'];
      return resultData;
    } else {
      return {};
    }
  }

  Future<bool> changeUserDetail({required int id, required String token, required Map<String, dynamic> body}) async {
    var result = await HttpService.dioCall(token: token).post('/user/update/$id', data: body);
    if (result.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<Map<String, dynamic>>> getPromo() async {
    var token = await Hive.box('venturo').get('token');
    var result = await HttpService.dioCall(token: token).get('/promo/type/diskon');
    if (result.statusCode == 200) {
      return (result.data['data'] as List).map((e) => e as Map<String, dynamic>).toList();
    } else {
      return [];
    }
  }

  Future<Map<String, dynamic>> getListOfData({int offset = 0}) async {
    var dataItem = await getData();
    int limit = 10 + offset;
    if (limit > dataItem.length) limit = dataItem.length;
    return {
      'data': dataItem.getRange(offset, limit).toList(),
      'next': limit < dataItem.length ? true : null,
      'previous': offset > 0 ? true : null,
    };
  }
}