import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';

import '../../repositories/list_repository.dart';

class MenuCard extends StatelessWidget {
  final Map<String, dynamic> menu;
  final void Function()? onTap;
  final void Function()? onAdd;
  final void Function()? onRemove;
  final int total;

  const MenuCard({Key? key, required this.menu, this.onTap, this.onAdd, this.onRemove,required this.total}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Ink(
      padding: EdgeInsets.all(7.r),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.r),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 90.h,
            width: 90.w,
            margin: EdgeInsets.only(right: 12.r),
            padding: EdgeInsets.all(5.r),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.r),
              color: Colors.grey[100]
            ),
            child: Builder(
              builder: (context) {
                String? photo;
                bool isError = false;
                for (var element in ListRepository().imageUrlError) {
                  if (element == menu['foto']) {
                    isError = true;
                    break;
                  }
                }
                if (!isError) {
                  photo = menu['foto'];
                }
                return CachedNetworkImage(
                  imageUrl: photo ?? 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/240px-No_image_available.svg.png',
                  fit: BoxFit.contain,
                  errorWidget: (context, url, error) => CachedNetworkImage(
                    imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/240px-No_image_available.svg.png',
                    useOldImageOnUrlChange: true,
                    fit: BoxFit.contain,
                  ),
                  placeholder: (context, url) => const CircularProgressIndicator()
                );
              },
            )
          ),
          Expanded(
            flex: 2,
            child: InkWell(
              onTap: onTap,
              child: Ink(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      menu['nama'] ?? menu['name'],
                      style: Get.textTheme.titleMedium,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                    Text(
                      'Rp.${menu['harga']}',
                      style: Get.textTheme.bodyMedium!.copyWith(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      'Tambahkan Catatan',
                      style: Get.textTheme.labelSmall,
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            )
          ),
          Expanded(
            child: SizedBox(
              height: double.maxFinite,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Builder(builder: (context) {
                    if (total != 0) {
                      return Material(
                        clipBehavior: Clip.antiAlias,
                        borderRadius: BorderRadius.circular(4),
                        child: InkWell(
                          onTap: onRemove,
                          child: Ink(
                            padding: EdgeInsets.all(2.r),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.r),
                              border: Border.all(
                                color: Theme.of(context).primaryColor, width: 2.r),
                            ),
                            child: Icon(Icons.remove, size: 20.r, color: MainColor.primary),
                          ),
                        ),
                      );
                    } else {
                      return 5.horizontalSpace;
                    }
                  }),
                  5.horizontalSpace,
                  Builder(builder: (context) {
                      if (total != 0) {
                        return Expanded(
                          child: Text('$total',
                            style: Get.textTheme.labelLarge!.copyWith(fontSize: 13, color: Theme.of(context).primaryColor),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          )
                        );
                      } else {
                        return 5.horizontalSpace;
                      }
                    },
                  ),
                  5.horizontalSpace,
                  Material(
                    clipBehavior: Clip.antiAlias,
                    borderRadius: BorderRadius.circular(4),
                    child: InkWell(
                      onTap: onAdd,
                      child: Ink(
                        padding: EdgeInsets.all(2.r),
                        color: Theme.of(context).primaryColor,
                        child: Icon(Icons.add, size: 20.r, color: Colors.white),
                      ),
                    ),
                  ),
                  5.horizontalSpace
                ],
              ),
            )
          ),
        ],
      ),
    );
  }
}