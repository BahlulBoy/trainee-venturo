import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';

class MenuChip extends StatelessWidget{
  final bool isSelected;
  final String text;
  final Function()? onTap;
  final IconData icon;

  const MenuChip({Key? key, required this.text, this.isSelected = false, required this.icon, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(30.r),
      child: Ink(
        padding: EdgeInsets.symmetric(horizontal: 14.r),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.r),
          color: isSelected ? Colors.black : Theme.of(context).primaryColor,
          boxShadow: const [
            BoxShadow(
              offset: Offset(0, 2),
              blurRadius: 8,
              spreadRadius: -1,
              color: Colors.black54,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  icon,
                  color: MainColor.white,
                ),
                8.horizontalSpace,
                Text(
                  text,
                  style: Get.textTheme.bodyLarge!.copyWith(
                    color: MainColor.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}