import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/modules/features/beranda/controllers/beranda_controller.dart';
import 'package:trainee/modules/features/beranda/views/components/order_item_card.dart';

class OnGoingOrderTabView extends StatelessWidget{
  const OnGoingOrderTabView({super.key});
  static FirebaseAnalytics  analytics = FirebaseAnalytics.instance;
  
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SmartRefresher(
        controller: BerandaController.to.onGoingRefreshController,
        enablePullDown: true,
        enablePullUp: true,
        onRefresh: BerandaController.to.getOngoingOrders,
        onLoading: BerandaController.to.onLoadMoreOngoing,
        child: ListView.separated(
          padding: EdgeInsets.all(25.r),
          itemBuilder: (context, index) => OrderItemCard(
            order: BerandaController.to.onGoingOrdersList[index],
            onTap: () async {
              var b = await Get.toNamed(
                '${MainRoute.order}/${BerandaController.to.onGoingOrdersList[index].idOrder}',
              );
              if (b != null) {
                log(b);
                BerandaController.to.onGoingRefreshController.requestRefresh();
              }
            },
            onOrderAgain: () {},
          ), 
          separatorBuilder: (context, index) => 16.verticalSpace, 
          itemCount: BerandaController.to.onGoingOrdersList.length,
        )
      )
    );
  }
}