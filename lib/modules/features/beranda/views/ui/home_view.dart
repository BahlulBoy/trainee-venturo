import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trainee/modules/features/beranda/controllers/beranda_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainee/modules/features/beranda/repositories/list_repository.dart';
import 'package:trainee/modules/features/beranda/views/components/menu_card.dart';
import 'package:trainee/modules/features/beranda/views/components/menu_chip.dart';
import 'package:trainee/modules/features/beranda/views/components/promo_card.dart';
import 'package:trainee/modules/features/beranda/views/components/search_app_bar.dart';
import 'package:trainee/modules/features/beranda/views/components/section_header.dart';

import '../../../../../configs/routes/main_route.dart';

class HomeView extends StatelessWidget{
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      physics: const ClampingScrollPhysics(),
      headerSliverBuilder: (context, innerBoxIsScrolled) {
        return [
          SliverToBoxAdapter(
            child: SearchAppBar(
              onChange: (value) => BerandaController.to.keyword(value),
            ),
          ),
          SliverToBoxAdapter(child: 22.verticalSpace),
          // list of promo
          SliverToBoxAdapter(
            child: SectionHeader(
              icon: Icons.note_alt_outlined,
              title: 'Available promo'.tr,
            ),
          ),
          SliverToBoxAdapter(child: 22.verticalSpace),
          SliverToBoxAdapter(
            child: SizedBox(
              width: 1.sw,
              height: 188.h,
              child: Obx(
                () => ListView.separated(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  itemBuilder: (context, index) {
                    return PromoCard(
                      onTap: () {
                        Get.toNamed(MainRoute.detailPromo, arguments: BerandaController.to.promo[index]['id_promo']);
                      },
                      enableShadow: false,
                      promoName: 'Promo ${BerandaController.to.promo[index]['nama']}',
                      discountNominal: '${BerandaController.to.promo[index]['diskon']}',
                      thumbnailUrl:
                        "https://javacode.landa.id/img/promo/gambar_62661b52223ff.png",
                    );
                  },
                  separatorBuilder: (context, index) => 26.horizontalSpace,
                  itemCount: BerandaController.to.promo.length,
                ),
              )
            ),
          ),
          SliverToBoxAdapter(child: 22.verticalSpace),
          // Row of categories
          SliverToBoxAdapter(
            child: SizedBox(
              width: 1.sw,
              height: 45.h,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: BerandaController.to.categories.length,
                padding: EdgeInsets.symmetric(horizontal: 25.w),
                itemBuilder: (context, index) {
                  final category = BerandaController.to.categories[index];
                  return Obx(() => MenuChip(
                    onTap: () {
                      BerandaController.to
                          .selectedCategory(category.toLowerCase());
                    },
                    isSelected:
                        BerandaController.to.selectedCategory.value == category.toLowerCase(),
                    text: category,
                    icon: BerandaController.to.iconList[index],
                  ));
                },
                separatorBuilder: (context, index) => 13.horizontalSpace,
              ),
            ),
          ),
          SliverToBoxAdapter(child: 10.verticalSpace),
        ];
      }, 
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Obx(() {
            final currentCategory =
                BerandaController.to.selectedCategory.value;
            return Container(
              width: 1.sw,
              height: 35.h,
              color: Colors.grey[100],
              margin: EdgeInsets.only(bottom: 10.h),
              child: SectionHeader(
                title: currentCategory == 'all'
                    ? 'All Menu'
                    : currentCategory == 'food'
                        ? 'Food'
                        : 'Drink',
                icon: currentCategory == 'all'
                    ? Icons.menu_book
                    : currentCategory == 'food'
                        ? Icons.food_bank
                        : Icons.local_drink,
              ),
            );
          }),
          Expanded(
            child: Obx(
              () => SmartRefresher(
                controller: BerandaController.to.refreshController,
                enablePullDown: true,
                onRefresh: BerandaController.to.onRefresh,
                enablePullUp:
                    BerandaController.to.canLoadMore.isTrue ? true : false,
                onLoading: BerandaController.to.getListOfData,
                child: ListView.builder(
                  padding: EdgeInsets.symmetric(horizontal: 25.w),
                  itemBuilder: (context, index) {
                    final item = BerandaController.to.filteredList[index];
                    for (var element in ListRepository().imageUrlError) {
                      if (element == item['foto']) {
                        item['foto'] = null;
                      }
                    }
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.5.h),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.r),
                        elevation: 2,
                        child: Obx(() {
                            var total = 0;
                            var indexItem = BerandaController.to.selectedItems.indexWhere((i) {
                              return i['id_menu'] == item['id_menu'];
                            });
                            if (indexItem != -1) {
                              total = BerandaController.to.selectedItems[indexItem]['jumlah'];
                            }
                            return MenuCard( 
                              menu: item,
                              onTap: () => BerandaController.to.goToDetail(item),
                              onAdd: () {
                                BerandaController.to.addItem(item);
                              },
                              onRemove: () {
                                BerandaController.to.deleteItem(item['id_menu']);
                              },
                              total: total,
                            );
                          }
                        )
                      ),
                    );
                  },
                  itemCount: BerandaController.to.filteredList.length,
                  itemExtent: 112.h,
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
}