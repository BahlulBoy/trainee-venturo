import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/modules/features/beranda/controllers/beranda_controller.dart';
import 'package:trainee/modules/features/beranda/views/components/date_picker.dart';
import 'package:trainee/modules/features/beranda/views/components/drop_down_status.dart';
import 'package:trainee/modules/features/beranda/views/components/order_item_card.dart';

import '../../../../../configs/routes/main_route.dart';

class OrderHistoryTabView extends StatelessWidget{
  const OrderHistoryTabView({super.key});
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        child: Obx(
          () => ConditionalSwitch.single(
            context: context, 
            valueBuilder: (context) =>
                BerandaController.to.orderHistoryState.value,
            caseBuilders: {},
            fallbackBuilder: (context) => CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 25.w, vertical: 25.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: DropdownStatus(
                            items: BerandaController.to.dateFilterStatus,
                            selectedItem:
                                BerandaController.to.selectedCategory.value,
                            onChanged: (value) => BerandaController.to
                                .setDateFilter(category: value),
                          ),
                        ),
                        22.horizontalSpaceRadius,
                        Expanded(
                          child: DatePicker(
                            selectedDate:
                                BerandaController.to.selectedDateRange.value,
                            onChanged: (value) =>
                                BerandaController.to.setDateFilter(range: value),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Conditional.single(
                  context: context,
                  conditionBuilder: (context) =>
                      BerandaController.to.filteredHistoryOrder.isNotEmpty,
                  widgetBuilder: (context) => SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) => Padding(
                          padding: EdgeInsets.only(bottom: 16.r),
                          child: OrderItemCard(
                            order:
                              BerandaController.to.filteredHistoryOrder[index],
                            onOrderAgain: () async {
                              await BerandaController.to.OrderAgain(BerandaController.to.filteredHistoryOrder[index]);
                            },
                            onTap: () =>
                              Get.toNamed('${MainRoute.order}/${BerandaController.to.filteredHistoryOrder[index].idOrder}',),
                          ),
                        ),
                        childCount:
                            BerandaController.to.filteredHistoryOrder.length,
                      ),
                    ),
                  ),
                  fallbackBuilder: (context) => const SliverToBoxAdapter(
                    child: SizedBox(),
                  ),
                )
              ],
            ),
          )
        ), 
        onRefresh: () => BerandaController.to.getOrderHistories()
      ),
      bottomNavigationBar: Container(
        width: 1.sw,
        height: 60.h,
        padding: EdgeInsets.symmetric(horizontal: 22.w, vertical: 15.h),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(30.r),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Total orders'.tr,
              style: Get.textTheme.titleLarge?.copyWith(
                fontWeight: FontWeight.w700,
              ),
            ),
            5.horizontalSpace,
            Obx(() => Text(
                  'Rp ${BerandaController.to.totalHistoryOrder}',
                  style: Get.textTheme.titleLarge?.copyWith(
                    fontWeight: FontWeight.w700,
                    color: Theme.of(context).primaryColor,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}