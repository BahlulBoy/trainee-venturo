import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/constants/cores/assets/image_constant.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trainee/modules/features/beranda/controllers/beranda_controller.dart';
import 'package:trainee/modules/features/checkout/views/components/tile_option.dart';

class ProfileView extends StatelessWidget{
  const ProfileView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        elevation: 2,
        backgroundColor: MainColor.white,
        centerTitle: true,
        title: Text(
         'Profile'.tr,
         style: Get.textTheme.titleMedium,
       ),
       shape: RoundedRectangleBorder(
         borderRadius: BorderRadius.vertical(
           bottom: Radius.circular(30.r),
         ),
       ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ImageConstant.bgPattern2),
            fit: BoxFit.fitHeight,
            alignment: Alignment.center
          ),
        ),
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 25.r, vertical: 25.r),
          children: [
            Center(
              child: Container(
                width: 170.r,
                height: 170.r,
                clipBehavior: Clip.antiAlias,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle
                ),
                child: Stack(
                  children: [
                    Obx(
                      () => Conditional.single(
                        context: context, 
                        conditionBuilder: (context) => BerandaController.to.imageFile != null, 
                        widgetBuilder: (context) => Image.file(
                          BerandaController.to.imageFile!,
                          width: 170.r,
                          height: 170.r,
                          fit: BoxFit.cover,
                        ), 
                        fallbackBuilder: (context) => Image.asset(
                          ImageConstant.bgProfile,
                          width: 170.r,
                          height: 170.r,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Material(
                        color: MainColor.primary,
                        child: InkWell(
                          onTap: BerandaController.to.pickImage,
                          child: Container(
                            width: double.infinity,
                            padding: EdgeInsets.only(top: 10.r, bottom: 15.r),
                            child: Text("Change".tr,
                              style: Get.textTheme.labelMedium!.copyWith(
                                color: MainColor.white
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            21.verticalSpacingRadius,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  ImageConstant.icKtp,
                ),
                7.horizontalSpaceRadius,
                Text(
                  'Verify your ID card now!'.tr,
                  style: Get.textTheme.labelMedium!
                      .copyWith(color: Colors.blue),
                )
              ],
            ),
            15.verticalSpace,
            SizedBox(
              width: double.infinity,
              child: Text('Account info'.tr,
                style: Get.textTheme.displayMedium!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: MainColor.primary
                ),
              ),
            ),
            20.verticalSpace,
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
              decoration: BoxDecoration(
                color: MainColor.background,
                borderRadius: BorderRadius.circular(10)
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Obx(
                    () => TileOption(
                      title: "Name".tr, 
                      message: BerandaController.to.user.value['nama'] ?? '-',
                      onTap: () {
                        BerandaController.to.updateProfileData("Name", 'nama');
                      },
                    ),
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  Obx(
                    () => TileOption(
                      title: "Birth date".tr, 
                      message: BerandaController.to.user.value['tgl_lahir'] ?? '-',
                      onTap: BerandaController.to.updateBirthDate,
                    ),
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  Obx(
                    () => TileOption(
                      title: "Phone number".tr, 
                      message: BerandaController.to.user.value['telepon'] ?? '-',
                      onTap: () {
                        BerandaController.to.updateProfileData("Phone number", 'telepon');
                      },
                    ),
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  Obx(
                    () => TileOption(
                      title: "Email".tr, 
                      message: BerandaController.to.user.value['email'] ?? '-',
                      onTap: () {
                        BerandaController.to.updateProfileData("Email", 'email');
                      },
                    ),
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  TileOption(
                    title: "Change PIN".tr, 
                    message: BerandaController.to.user.value['pin'] ?? '********',
                    onTap: () {
                      BerandaController.to.updateProfileData("Change PIN", 'pin');
                    },
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  TileOption(
                    title: "Change language".tr, 
                    message: BerandaController.to.currentLang.value,
                    onTap: BerandaController.to.updateLanguage,
                  ),
                ],
              ),
            ),
            15.verticalSpace,
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
              decoration: BoxDecoration(
                color: MainColor.background,
                borderRadius: BorderRadius.circular(10)
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TileOption(
                    title: "Penilaian", 
                    message: "",
                    icon: Icons.reviews,
                    onTap: () {},
                  ),
                ],
              ),
            ),
            15.verticalSpace,
            SizedBox(
              width: double.infinity,
              child: Text('Info Lainnya',
                style: Get.textTheme.displayMedium!.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: MainColor.primary
                ),
              ),
            ),
            20.verticalSpace,
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
              decoration: BoxDecoration(
                color: MainColor.background,
                borderRadius: BorderRadius.circular(10)
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TileOption(
                    title: "Device Info", 
                    message: BerandaController.to.deviceModel.value,
                    onTap: () {},
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  TileOption(
                    title: "Version", 
                    message: BerandaController.to.deviceVersion.value,
                    onTap: () {},
                  ),
                  Divider(color: Colors.black26, height: 2.h, thickness: 0.4),
                  TileOption(
                    title: "Privacy policy".tr, 
                    message: '',
                    onTap: () {
                      Get.toNamed(MainRoute.privacyPolicy);
                    },
                  ),
                ],
              ),
            ),
            20.verticalSpace,
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 60),
              child: ElevatedButton(
                onPressed: BerandaController.to.logoutAccount, 
                style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                  backgroundColor: MaterialStateProperty.all<Color>(MainColor.primary)
                ),
                child: const Text("Logout")
              ),
            )
          ]
        ),
      ),
    );
  }
}