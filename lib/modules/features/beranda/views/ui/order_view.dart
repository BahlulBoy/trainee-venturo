import 'package:flutter/material.dart';
import 'package:trainee/modules/features/beranda/views/components/order_top_bar.dart';
import 'package:trainee/modules/features/beranda/views/ui/ongoing_order_tab_view.dart';
import 'package:trainee/modules/features/beranda/views/ui/order_history_tab_view.dart';

class OrderView extends StatelessWidget{
  const OrderView({super.key});

  @override
  Widget build(BuildContext context) {
    return const DefaultTabController(
      length: 2, 
      child: SafeArea(
        child: Scaffold(
          appBar: OrderTopBar(),
          body: TabBarView(
            children: [
              OnGoingOrderTabView(),
              OrderHistoryTabView()
            ],
          ),
        )
      )
    );
  }
}