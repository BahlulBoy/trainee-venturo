import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/beranda/controllers/beranda_controller.dart';
import 'package:trainee/modules/features/beranda/views/ui/home_view.dart';
import 'package:trainee/modules/features/beranda/views/ui/order_view.dart';
import 'package:trainee/modules/features/beranda/views/ui/profile_view.dart';
import 'package:trainee/modules/global_controllers/global_controller.dart';

class Beranda extends StatelessWidget{
  const Beranda({super.key});
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  static const List<Widget> _pages = <Widget>[
    HomeView(),
    OrderView(),
    ProfileView()
  ];
  
  @override
  Widget build(BuildContext context) {
    GlobalController.to.checkConnection();
    analytics.setCurrentScreen(
      screenName: 'Beranda',
      screenClassOverride: 'Trainee'
    );
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        body: Obx(() => 
          _pages.elementAt(BerandaController.to.selectedIndexBottomBar.value)
        ),
        bottomNavigationBar: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)
          ),
          child: Obx(
            () => BottomNavigationBar(
              selectedFontSize: 10,
              unselectedFontSize: 10,
              backgroundColor: MainColor.black,
              selectedItemColor: MainColor.white,
              unselectedItemColor: MainColor.grey,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: const Icon(Icons.home),
                  label: 'Homepage'.tr,
                ),
                BottomNavigationBarItem(
                  icon: const Icon(Icons.table_restaurant_rounded),
                  label: 'Order'.tr,
                ),
                BottomNavigationBarItem(
                  icon: const Icon(Icons.account_circle_outlined),
                  label: 'Profile'.tr,
                ),
              ],
              currentIndex: BerandaController.to.selectedIndexBottomBar.value,
              onTap: (value) {
                BerandaController.to.changeIndexBottomBar(value);
              },
            ),
          ),
        ),
        floatingActionButton: Obx(() => 
          Opacity(
            opacity: BerandaController.to.selectedItems.isNotEmpty ? 1 : 0,
            child: FloatingActionButton(
              onPressed: () {
                if (BerandaController.to.selectedItems.isNotEmpty) {
                  BerandaController.to.toCheckout();
                }
              },
              child: const Icon(BootstrapIcons.cart),
            ),
          )
        ),
      ),
    );
  }
}