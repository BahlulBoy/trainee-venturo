// ignore_for_file: list_remove_unrelated_type, depend_on_referenced_packages
import 'dart:developer';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:trainee/configs/localization/localization.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/beranda/repositories/list_repository.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainee/modules/features/beranda/repositories/order_repository.dart';
import 'package:trainee/modules/features/beranda/views/components/name_bottom_sheet.dart';
import 'package:trainee/shared/widgets/image_picker_dialog.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:hive/hive.dart';
import 'package:trainee/utils/services/http_service.dart';

import '../modules/order_model.dart';
import '../views/components/language_bottomsheet_dialog.dart';

class BerandaController extends GetxController{
  static BerandaController get to => Get.find<BerandaController>();
  final RxInt selectedIndexBottomBar = 0.obs;

  @override
  void onInit() async {
    super.onInit();
    // Beranda
    repository = ListRepository();
    await getListOfData();
    await getPromo();
    // Beranda

    //OrderView
    _orderRepository = OrderRepository();
    getOngoingOrders();
    getOrderHistories();
    //OrderView

    //ProfileView
    await setUpInfoProfile();
    getDeviceInformation();
    //ProfileView
  }

  void changeIndexBottomBar(int index) {
    selectedIndexBottomBar.value = index;
  }

  // ------------------------------------------------------- //
  // ----------------------- Beranda ----------------------- //
  // ------------------------------------------------------- //
  late final ListRepository repository;
  final RxInt page = 0.obs;
  final RxList<Map<String, dynamic>> items = 
    <Map<String, dynamic>>[].obs;
  final RxList<Map<String, dynamic>> selectedItems =
    <Map<String, dynamic>>[].obs;
  final RxList<Map<String, dynamic>> promo = 
    <Map<String, dynamic>>[].obs;
  final RxBool canLoadMore = true.obs;
  final RxString selectedCategory = 'all'.obs;
  final RxString keyword = ''.obs;
  final List<String> categories = [
    'all',
    'makanan',
    'minuman',
  ];
  final RefreshController refreshController = RefreshController(initialRefresh: false);

  List<IconData> iconList = [
    Icons.table_chart, Icons.fastfood_rounded, Icons.local_drink
  ];
  

  void onRefresh() async {
    page(0);
    canLoadMore(true);
    final result = await getListOfData();
    if (result) {
      refreshController.refreshCompleted();
    } else {
      refreshController.refreshFailed();
    }
  }

  void goToDetail(Map<String, dynamic> item) async {
    var indexCheckData = BerandaController.to.selectedItems.indexWhere(
      (element) => element['id_menu'] == item['id_menu']
    );

    if (indexCheckData != -1) {
      var argument = BerandaController.to.selectedItems[indexCheckData];
      var data = await Get.toNamed(MainRoute.detailMenu, arguments: argument);
      if (data!= null && data.containsKey('id_menu')) {
        if (data['jumlah'] != 0) {
          BerandaController.to.selectedItems[indexCheckData] = data;
        } else {
          BerandaController.to.selectedItems.remove(indexCheckData);
        }
      }
    } else {
      var argument = {
        'id_menu' : item['id_menu']
      };
      var dataInput = await Get.toNamed(MainRoute.detailMenu, arguments: argument);
      if (dataInput != null && dataInput.containsKey('id_menu')) {
        if (dataInput['jumlah'] != 0) {
          selectedItems.add(dataInput);
        }
      }
    }
  }

  void addItem(Map<String, dynamic> data) async {
    var indexSelect = selectedItems.indexWhere((element) => data['id_menu'] == element['id_menu']);
    if (indexSelect == -1) {
      var bodyData = {
        'id_menu' : data['id_menu'],
        "harga": data['harga'],
        "toping": [
        ],
        "jumlah": 1,
        'item' : data
      };
      selectedItems.add(bodyData);
    } else {
      selectedItems[indexSelect]['jumlah']++;
    }
    selectedItems.refresh();
  }

  void deleteItem(int id) {
    var indexToRemove = selectedItems.indexWhere((item) {
      return item['id_menu'] == id;
    });
    if (indexToRemove != -1) {
      if (selectedItems[indexToRemove]['jumlah'] <= 1) {
        selectedItems.removeAt(indexToRemove);
      } else {
        selectedItems[indexToRemove]['jumlah']--;
      }
    }
    selectedItems.refresh();
  }

  List<Map<String, dynamic>> get filteredList => items
    .where((element) => element['nama']
      .toString()
      .toLowerCase()
      .contains(
        keyword.value.toLowerCase()
        ) && (
        selectedCategory.value == 'all' || element['kategori'] == selectedCategory.value
      ))
      .toList();

  Future<void> getPromo() async {
    var g = await repository.getPromo();
    promo.value = g;
    log(g.toString());
  }

  Future<bool> getListOfData() async {
    try {
      final result = await repository.getListOfData(
        offset: page.value * 10,
      );

      if (result['previous'] == null) {
        items.clear();
      }

      if (result['next'] == null) {
        canLoadMore(false);
        refreshController.loadNoData();
      }

      final r = (result['data'] as List).map((e) => e as Map<String, dynamic>).toList();
      items.addAll(r.toList());
      page.value++;
      refreshController.loadComplete();
      return true;
    } catch (exception, stacktrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stacktrace,
      );
      refreshController.loadFailed();
      return false;
    }
  }

  void toCheckout() async {
    var result = await Get.toNamed(MainRoute.checkout, arguments: BerandaController.to.selectedItems);
    if (result != null) {
      selectedItems.value = [];
    }
  }
  // ------------------------------------------------------- //
  // ----------------------- Beranda ----------------------- //
  // ------------------------------------------------------- //


  // ------------------------------------------------------- //
  // ---------------------- OrderView ---------------------- //
  // ------------------------------------------------------- //
  late final OrderRepository _orderRepository;
  final RefreshController onGoingRefreshController = RefreshController();
  RxList<DataOrder> onGoingOrders = <DataOrder>[].obs;
  RxList<DataOrder> historyOrders = <DataOrder>[].obs;
  RxString onGoingOrderState = 'loading'.obs;
  RxString orderHistoryState = 'loading'.obs;

  RxList<DataOrder> onGoingOrdersList = <DataOrder>[].obs;
  RxInt pageOngoing = 0.obs;

  Rx<String> selectedCategoryOrder = 'all'.obs;

  Map<String, String> get dateFilterStatus => {
        'all': 'All status'.tr,
        'completed': 'Completed'.tr,
        'canceled': 'Canceled'.tr,
      };

  Rx<DateTimeRange> selectedDateRange = DateTimeRange(
    start: DateTime.now().subtract(const Duration(days: 30)),
    end: DateTime.now(),
  ).obs;

  void onLoadMoreOngoing() {
    if (onGoingOrders.length <= onGoingOrdersList.length) {
      onGoingRefreshController.loadNoData();
    } else {
      var limit = 10 + pageOngoing.value - 1;
      if (limit > onGoingOrders.length) limit = onGoingOrders.length;
      var data = onGoingOrders.getRange(pageOngoing.value, limit).toList();
      onGoingOrdersList.addAll(data);
      onGoingOrdersList.refresh();
      pageOngoing.value = limit;
      onGoingRefreshController.loadComplete();
    }
  }

  Future<void> getOngoingOrders() async {
    onGoingOrdersList.clear();
    pageOngoing.value = 0;
    onGoingOrderState('loading');
    try {
      List<DataOrder> result = await _orderRepository.getOngoingOrder();
      List<DataOrder> data = result.where((element) => element.status != 4).toList();
      onGoingOrders(data.reversed.toList());
      onGoingOrderState('success');
      onLoadMoreOngoing();
      onGoingRefreshController.refreshCompleted();
    } catch (exception, stacktrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stacktrace,
      );
      onGoingOrderState('error');
      onGoingRefreshController.refreshFailed();
    }
  }

  Future<bool> getOrderHistories() async {
    orderHistoryState('loading');
    try {
      final result = await _orderRepository.getOrderHistory();
      historyOrders(result.toList());
      orderHistoryState('success');
      return true;
    } catch (exception, stacktrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stacktrace,
      );
      orderHistoryState('error');
      return false; 
    }
  }

  Future<void> OrderAgain(DataOrder data) async {
    var token = await Hive.box('venturo').get('token');
    List<Map<String, dynamic>> y = [];
    data.menu!.forEach(
      (element) { 
        var h = {
          'id_order' : element.idMenu,
          'harga' : element.harga,
          'jumlah' : element.jumlah
        }; 
        y.add(h);
      }
    );
    for (var element in y) {
      var t = await HttpService.dioCall(token: token).get('/menu/detail/${element['id_order']}');
      if (t.statusCode == 200) {
        element['item'] = t.data['data']['menu'];;
      }
    }
    BerandaController.to.selectedItems.addAll(y);
    Get.toNamed(MainRoute.checkout, arguments: selectedItems);
  }

  void setDateFilter({String? category, DateTimeRange? range}) {
    selectedCategory(category);
    selectedDateRange(range);
  }

  List<DataOrder> get filteredHistoryOrder {
    final historyOrderList = historyOrders.toList();


    if (selectedCategory.value == 'canceled') {
      historyOrderList.removeWhere((element) => element.status != 4);
    } else if (selectedCategory.value == 'completed') {
      historyOrderList.removeWhere((element) => element.status != 3);
    }


    historyOrderList.removeWhere((element) =>
        DateTime.parse(element.tanggal as String)
            .isBefore(selectedDateRange.value.start) ||
        DateTime.parse(element.tanggal as String)
            .isAfter(selectedDateRange.value.end));


    historyOrderList.sort((a, b) => DateTime.parse(b.tanggal as String)
        .compareTo(DateTime.parse(a.tanggal as String)));


    return historyOrderList;
  }

  String get totalHistoryOrder {
    final total = filteredHistoryOrder.where((e) => e.tanggal == 3.toString()).fold(
        0,
        (previousValue, element) =>
            previousValue + element.totalBayar!);
    return total.toString();
  }
  // ------------------------------------------------------- //
  // ---------------------- OrderView ---------------------- //
  // ------------------------------------------------------- //

  // ------------------------------------------------------- //
  // ----------------------- Profile ----------------------- //
  // ------------------------------------------------------- //
  final Rx<File?> _imageFile = Rx<File?>(null);
  RxString deviceModel = ''.obs;
  RxString deviceVersion = ''.obs;
  RxString currentLang = Localization.currentLanguage.obs;
  Rx<Map<String, dynamic>> user = Rx<Map<String, dynamic>>({});

  Future<void> setUpInfoProfile() async {
    final box = Hive.box('venturo');
    var name = await box.get('name');
    var email = await box.get('email');
    var noTelepon = await box.get('no_telepon');
    var tglLahir = await box.get('tgl_lahir');
    var imagePath = await box.get('foto');
    if (imagePath != null) {
      _imageFile.value = File(imagePath); 
    }
    await updateUser(nama: name, email: email, telepon: noTelepon, tglLahir: tglLahir);
  }

  File? get imageFile => _imageFile.value;

  Future<void> pickImage() async {
    ImageSource? imageSource = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const ImagePickerDialog()
    );

    if (imageSource == null) return;

    final pickedFile = await ImagePicker().pickImage(
      source: imageSource,
      maxWidth: 300,
      maxHeight: 300,
      imageQuality: 100,
    );

    if (pickedFile != null) {
      _imageFile.value = File(pickedFile.path);
      final croppedFile = await ImageCropper()
        .cropImage(sourcePath: _imageFile.value!.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square
        ],
        uiSettings: [
          AndroidUiSettings(
            toolbarTitle: 'Cropper'.tr,
            toolbarColor: MainColor.primary,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.square,
            lockAspectRatio: true,
          )
        ]
      );

      if (croppedFile != null)  {
        _imageFile.value = File(croppedFile.path);
      }
    }

    if (_imageFile.value != null) {
      await Hive.box('venturo').put('foto', _imageFile.value?.path);
      log('${_imageFile.value?.path}');
    }
  }

  Future<void> updateLanguage() async {
    String? language = await Get.bottomSheet(
      const LanguageBottomSheet(),
      backgroundColor: MainColor.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r)
        )
      ),
      isScrollControlled: true
    );

    if (language != null) {
      Localization.changeLocale(language);
      currentLang(language);
    }
  }

  Future<void> updateUser({
    String? nama,
    DateTime? tglLahir,
    String? telepon,
    String? email,
    String? pin,

  }) async {
    final reqData = <String, String>{};

    if (nama != null) reqData["nama"] = nama;
    if (tglLahir != null) {
      reqData["tgl_lahir"] = DateFormat('yyy-MM-dd').format(tglLahir);
    }
    if (telepon != null) reqData["telepon"] = telepon;
    if (email != null) reqData["email"] = email;
    if (pin != null) reqData["pin"] = pin;

    // update user data
    user.update((val) {
      val?.addAll(reqData);
    });
  }

  Future<void> updateProfileData(String title, String data) async {
    String? dataResult = await Get.bottomSheet(
      NameBottomSheet(nama: user.value[data] ?? '', title: title),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.r),
        ),
      ),
      isScrollControlled: true,
    );

    if (dataResult != null && dataResult.isNotEmpty) {
      var box = Hive.box('venturo');
      var token = await box.get('token');
      var id = await box.get('id');
      switch (title) {
        case "Name": 
          var body = {
            'nama' : dataResult
          };
          var result = await ListRepository().changeUserDetail(id: id, token: token, body: body);
          if (result) {
            await updateUser(nama: dataResult);
          } else {
            Get.snackbar('Error', 'Data tidak berubah');
          }
          break;
        case "Phone number":
          var body = {
            'telepon' : dataResult
          };
          var result = await ListRepository().changeUserDetail(id: id, token: token, body: body);
          if (result) {
            await updateUser(telepon: dataResult);
          } else {
            Get.snackbar('Error', 'Data tidak berubah');
          }
          break;
        case "Email":
          var body = {
            'email' : dataResult
          };
          var result = await ListRepository().changeUserDetail(id: id, token: token, body: body);
          if (result) {
            await updateUser(email: dataResult);
          } else {
            Get.snackbar('Error', 'Data tidak berubah');
          }
          break;
        case "Change PIN":
          await updateUser(pin: dataResult);
          break;
      }
    }
  }

  Future<void> updateBirthDate() async {
    DateTime? birthDate = await showDatePicker(
      context: Get.context!,
      initialDate: DateTime(DateTime.now().year - 21),
      firstDate: DateTime(DateTime.now().year - 100),
      lastDate: DateTime.now(),
    );

    if (birthDate != null) {
      await updateUser(tglLahir: birthDate);
    }
  }

  Future getDeviceInformation() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    deviceModel.value = androidInfo.model;
    deviceVersion.value = androidInfo.version.release;
  }

  Future<void> logoutAccount() async {
    EasyLoading.show(
      status: 'Sedang Diproses...',
      maskType: EasyLoadingMaskType.black,
      dismissOnTap: false,
    ); 
    var token = await Hive.box('venturo').get('token');
    var logoutResult = await HttpService.dioCall(token: token).get('/auth/logout');
    if (logoutResult.statusCode == 200) {
      await eraseDataAccount().whenComplete(
        () {
          EasyLoading.dismiss();
          Get.offAllNamed(MainRoute.signIn);
        }
      );
    }
  }

  Future<void> eraseDataAccount() async {
    await Hive.box('venturo').clear();
    await FirebaseAuth.instance.signOut();
  }
  // ------------------------------------------------------- //
  // ----------------------- Profile ----------------------- //
  // ------------------------------------------------------- //
}