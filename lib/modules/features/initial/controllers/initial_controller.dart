import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/modules/features/initial/views/ui/get_location_screen.dart';
import 'package:trainee/utils/services/location_service.dart';

class InitialController extends GetxController{
  static InitialController get to => Get.find();

  RxString statusLocation = RxString('loading');
  RxString messageLocation = RxString('');
  Rxn<Position> position = Rxn<Position>();
  RxnString address = RxnString();

  @override
  void onReady() {
    super.onReady();
    getLocation();
    LocationService.streamService.listen((event) => getLocation());
  }

  Future<void> getLocation() async {
    if (Get.isDialogOpen == false) {
      Get.dialog(const GetLocationScreen(), barrierDismissible: false);
    }

    try {
      statusLocation.value = 'loading';
      final locationResult = await LocationService.getCurrentPosition();

      if (locationResult.success) {
        position.value = locationResult.position;
        address.value = locationResult.address;
        statusLocation.value = 'success';

        await Future.delayed(const Duration(seconds: 4));
        Get.offAllNamed(MainRoute.beranda);
      } else {
        statusLocation.value = 'error';
        messageLocation.value = locationResult.message!;
      }
    } catch (e) {
      statusLocation.value = 'error';
      messageLocation.value = 'server error'.tr;
    }
  }
}