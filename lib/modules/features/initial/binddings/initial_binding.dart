import 'package:get/get.dart';
import 'package:trainee/modules/features/initial/controllers/initial_controller.dart';

class InitialBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(InitialController());
  }
}