import 'package:get/get.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/modules/global_controllers/global_controller.dart';
import 'package:trainee/utils/services/local_storage_service.dart';

class SplashScreenController extends GetxController{
  static SplashScreenController get to => Get.find<SplashScreenController>();

  Future<void> checkAuth() async {
    await GlobalController.to.checkConnection();
    var isHaveToken = await LocalStorageService.checkAuth();
    if (isHaveToken) {
      Get.offNamed(MainRoute.initial);
    } else {
      Get.offNamed(MainRoute.signIn);
    }
  }
}