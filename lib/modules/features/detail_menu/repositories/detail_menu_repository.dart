import 'package:hive_flutter/hive_flutter.dart';
import 'package:trainee/utils/services/http_service.dart';

class DetailMenuRepository {
  DetailMenuRepository();

  Future<dynamic> getDetailMenu(int id) async {
    var token = await Hive.box('venturo').get('token');
    var data = await HttpService.dioCall(token: token).get(
      '/menu/detail/$id'
    );
    return data.data;
  }
}