import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/detail_menu/controllers/detail_menu_controller.dart';

class DetailMenuView extends StatelessWidget{
  const DetailMenuView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: MainColor.background,
      appBar: AppBar(
        elevation: 10,
        backgroundColor: MainColor.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20.0), // Radius kiri bawah
            bottomRight: Radius.circular(20.0), // Radius kanan bawah
          ),
        ),
        leading: IconButton(
          iconSize: 23,
          onPressed: () {
            Get.back();
          }, 
          icon: const Icon(Icons.arrow_back_ios_new,
            color: MainColor.black,
          )
        ),
        title: Text('Detail Menu', 
          style: Get.textTheme.titleMedium!.copyWith(
            fontSize: 20,
            fontWeight: FontWeight.w500,
            color: MainColor.black
          ),
        ),
        centerTitle: true,
      ),
      body: SizedBox(
        width: 100.sw,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 50.w),
              width: 100.sw,
              height: 280.h,
              child: Obx(() {
                String? photo = DetailMenuController.to.photo.value;
                if (photo.isEmpty) {
                  photo = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/240px-No_image_available.svg.png';
                }
                if (DetailMenuController.to.menu.containsKey('foto')) {
                  return CachedNetworkImage(
                    imageUrl: photo,
                    fit: BoxFit.fitWidth,
                  );
                } else {
                  return CachedNetworkImage(
                    imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/240px-No_image_available.svg.png',
                    fit: BoxFit.fitHeight,
                  );
                }
              }),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(right: 25, left: 25, top: 27),
                width: 100.sw,
                decoration: const BoxDecoration(
                  color: MainColor.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40))
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Obx(() {
                              if (DetailMenuController.to.menu.isEmpty) {
                                return Text('',
                                  style: Get.textTheme.headlineSmall!.copyWith(
                                    color: MainColor.primary,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 20,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                );
                              } else {
                                return Text('${DetailMenuController.to.menu['nama']}',
                                  style: Get.textTheme.headlineSmall!.copyWith(
                                    color: MainColor.primary,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 20,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                );
                              }
                            })
                          ),
                          Expanded(
                            child: SizedBox(
                              width: double.maxFinite,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                InkWell(
                                  onTap: () => DetailMenuController.to.kurangJumlah(),
                                  child: Ink(
                                    child: Container(
                                      width: 22,
                                      height: 22,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        border: Border.all(color: Theme.of(context).primaryColor, width: 2)
                                      ),
                                      child: Text('-',
                                        textAlign: TextAlign.center,
                                        style: Get.textTheme.labelLarge!.copyWith(color: Theme.of(context).primaryColor),),
                                    ),
                                  ),
                                ),
                                10.horizontalSpace,
                                Obx(() {
                                  if (DetailMenuController.to.orderData.isEmpty) {
                                    return Text('0',
                                      style: Get.textTheme.labelLarge!.copyWith(fontSize: 15, color: Theme.of(context).primaryColor),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                    );
                                  } else {
                                    return Text('${DetailMenuController.to.orderData['jumlah']}',
                                      style: Get.textTheme.labelLarge!.copyWith(fontSize: 15, color: Theme.of(context).primaryColor),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                    );
                                  }
                                }),
                                10.horizontalSpace,
                                InkWell(
                                  onTap: () => DetailMenuController.to.tambahJumlah(),
                                  child: Ink(
                                    child: Container(
                                      width: 22,
                                      height: 22,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Theme.of(context).primaryColor
                                      ),
                                      child: Text('+',
                                        textAlign: TextAlign.center,
                                        style: Get.textTheme.labelLarge!.copyWith(color: MainColor.white),),
                                    ),
                                  ),
                                ),
                              ],
                              ),  
                            )
                          )
                        ],
                      ),
                      10.verticalSpace,
                      Container(
                        width: 100.sw,
                        padding: EdgeInsets.only(right: 80.w),
                        child: Obx(() {
                          if (DetailMenuController.to.menu.isEmpty) {
                            return Text('',
                              style: Get.textTheme.labelSmall,
                            );
                          } else {
                            return Text('${DetailMenuController.to.menu['deskripsi']}',
                              style: Get.textTheme.labelSmall,
                            );
                          }
                        }),
                      ),
                      10.verticalSpace,
                      const Divider(
                        thickness: 1,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5.h),
                        width: 100.sw,
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  const Icon(BootstrapIcons.cash_coin,
                                    size: 13,
                                  ),
                                  10.horizontalSpace,
                                  Text('Harga',
                                    style: Get.textTheme.labelMedium!.copyWith(
                                      fontSize: 13
                                    ),
                                  )
                                ],
                              )
                            ),
                            Expanded(
                              child: Obx(() {
                                if (DetailMenuController.to.menu.isEmpty) {
                                  return Text('Rp.0',
                                    style: Get.textTheme.labelMedium!.copyWith(
                                      fontSize: 13,
                                      color: MainColor.primary,
                                      fontWeight: FontWeight.w600
                                    ),
                                    textAlign: TextAlign.end,
                                  );
                                } else {
                                  return Text('Rp.${DetailMenuController.to.menu['harga']}',
                                    style: Get.textTheme.labelMedium!.copyWith(
                                      fontSize: 13,
                                      color: MainColor.primary,
                                      fontWeight: FontWeight.w600
                                    ),
                                    textAlign: TextAlign.end,
                                  );
                                }
                              }),
                            )
                          ],
                        ),
                      ),
                      const Divider(
                        thickness: 1,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5.h),
                        width: 100.sw,
                        child: InkWell(
                          onTap: () {
                            DetailMenuController.to.showBottomSheetLevel(context);
                          },
                          child: Ink(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      const Icon(BootstrapIcons.fire,
                                        size: 13,
                                      ),
                                      10.horizontalSpace,
                                      Text('Level',
                                        style: Get.textTheme.labelMedium!.copyWith(
                                          fontSize: 13
                                        ),
                                      )
                                    ],
                                  )
                                ),
                                Expanded(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Obx(() {
                                        if (DetailMenuController.to.orderData.isEmpty || !DetailMenuController.to.orderData.containsKey('level') ) {
                                          return Text('',
                                            style: Get.textTheme.labelMedium!.copyWith(
                                              fontSize: 13
                                            ),
                                          );
                                        } else {
                                          return Text('${DetailMenuController.to.orderData['level']['keterangan']}',
                                            style: Get.textTheme.labelMedium!.copyWith(
                                              fontSize: 13
                                            ),
                                          );
                                        }
                                      }),
                                      5.horizontalSpace,
                                      const Icon(Icons.arrow_forward_ios,
                                        color: MainColor.grey,
                                        size: 13
                                      )
                                    ],
                                  )
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const Divider(
                        thickness: 1,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5.h),
                        width: 100.sw,
                        child: InkWell(
                          onTap: () {
                            DetailMenuController.to.showBottomSheetToping(context);
                          },
                          child: Ink(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      const Icon(BootstrapIcons.eyedropper,
                                        size: 13
                                      ),
                                      10.horizontalSpace,
                                      Text('Toping',
                                        style: Get.textTheme.labelMedium!.copyWith(
                                          fontSize: 13
                                        ),
                                      )
                                    ],
                                  )
                                ),
                                Expanded(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Expanded(
                                        child: Obx(() {
                                          if (DetailMenuController.to.orderData.isEmpty || !DetailMenuController.to.orderData.containsKey('topping') ) {
                                            return Text('',
                                              style: Get.textTheme.labelMedium!.copyWith(
                                                fontSize: 13,
                                              ),
                                              textAlign: TextAlign.end,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            );
                                          } else {
                                            var text = '';
                                            DetailMenuController.to.orderData['toping'];
                                            for (var i = 0; i < DetailMenuController.to.orderData['toping'].length; i++) {
                                              text += '${DetailMenuController.to.orderData['toping'][i]['keterangan']}, ';
                                            }
                                            return Text(text,
                                              style: Get.textTheme.labelMedium!.copyWith(
                                                fontSize: 13,
                                              ),
                                              textAlign: TextAlign.end,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            );
                                          }
                                        }),
                                      ),
                                      5.horizontalSpace,
                                      const Icon(Icons.arrow_forward_ios,
                                        color: MainColor.grey,
                                        size: 13
                                      )
                                    ],
                                  )
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const Divider(
                        thickness: 1,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5.h),
                        width: 100.sw,
                        child: InkWell(
                          onTap: () {
                            DetailMenuController.to.showBottomSheetCatatan(context);
                          },
                          child: Ink(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      const Icon(BootstrapIcons.pencil_fill,
                                        size: 13
                                      ),
                                      10.horizontalSpace,
                                      Text('Catatan',
                                        style: Get.textTheme.labelMedium!.copyWith(
                                          fontSize: 13
                                        ),
                                      )
                                    ],
                                  )
                                ),
                                Expanded(
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Expanded(
                                        child: Obx(() {
                                        if (DetailMenuController.to.orderData.isEmpty || !DetailMenuController.to.orderData.containsKey('catatan') ) {
                                            return Text('',
                                              style: Get.textTheme.labelMedium!.copyWith(
                                                fontSize: 13,
                                              ),
                                              textAlign: TextAlign.end,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            );
                                          } else {
                                            return Text('${DetailMenuController.to.orderData['catatan']}',
                                              style: Get.textTheme.labelMedium!.copyWith(
                                                fontSize: 13,
                                              ),
                                              textAlign: TextAlign.end,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            );
                                          }
                                        }),
                                      ),
                                      5.horizontalSpace,
                                      const Icon(Icons.arrow_forward_ios,
                                        color: MainColor.grey,
                                        size: 13
                                      )
                                    ],
                                  )
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const Divider(
                        thickness: 1,
                      ),
                      10.verticalSpace,
                      Container(
                        width: 100.sw,
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: TextButton(
                          onPressed: () {
                            DetailMenuController.to.showBottomTest(context);
                          }, 
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(MainColor.primary),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text("Tambahkan Pesanan",
                              style: Get.textTheme.bodyMedium!.copyWith(
                                color: MainColor.white
                              ),
                            ),
                          )
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}