import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';

class TopingChip extends StatelessWidget{
  const TopingChip({Key? key, this.isSelected = false,  this.onTap, required this.title}) : super(key: key);

  final bool isSelected;
  final void Function()? onTap;
  final String title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Ink(
        child: Container(
          decoration: BoxDecoration(
            color: isSelected ? MainColor.primary : MainColor.white,
            borderRadius: BorderRadius.circular(25),
            border: Border.all(
              color: MainColor.primary
            )
          ),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
          child: Row(
            children: [
              Text(title,
                style: Get.textTheme.labelMedium!.copyWith(
                  fontSize: 15,
                  color: isSelected ? MainColor.white : MainColor.primary
                ),
              ),
              2.horizontalSpace,
              isSelected ? 
              const Icon(BootstrapIcons.check,
                color: MainColor.white,
              ) : Container(),
            ],
          ),
        ),
      ),
    );
  }

}