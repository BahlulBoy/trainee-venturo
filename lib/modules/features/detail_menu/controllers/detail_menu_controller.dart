import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/detail_menu/repositories/detail_menu_repository.dart';
import 'package:trainee/modules/features/detail_menu/views/components/toping_chip.dart';

import '../../beranda/repositories/list_repository.dart';

class DetailMenuController extends GetxController{
  static DetailMenuController get to => Get.find<DetailMenuController>();

  final RxMap<String, dynamic> menu = <String, dynamic>{}.obs;
  final RxString photo = ''.obs;
  final RxList<dynamic> toping = [].obs;
  final RxList<dynamic> level = [].obs;
  final RxMap<String, dynamic> orderData = <String, dynamic>{}.obs;

  final TextEditingController catatanController = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    EasyLoading.show(
      status: 'Loading...',
      maskType: EasyLoadingMaskType.black,
      dismissOnTap: false,
    ); 
    var arguments = await Get.arguments;
    var resultApi = await DetailMenuRepository().getDetailMenu(arguments['id_menu']);
    if (resultApi['status_code'] == 200) {
      menu.value = resultApi['data']['menu'];
      for (var element in ListRepository().imageUrlError) {
        if (element == menu['foto']) {
          photo('');
          break;
        } else {
          photo(menu['foto']);
        }
      }
      toping.value = resultApi['data']['topping'];
      level.value = resultApi['data']['level'];
      orderData.value = {
          "id_menu": arguments['id_menu'],
          "harga": menu['harga'],
          "toping": [
          ],
          "jumlah": 0,
          'item' : menu
      };

      if (arguments.containsKey('jumlah')) {
        orderData.value = arguments;
      }
      EasyLoading.dismiss();
    } else {
      EasyLoading.dismiss();
      Get.back(result: 'fail');
    }  
  }

  /// kembali ke halaman sebelumnya dengan membawa data order
  void submitValue() {
    var callbackData = orderData;
    if (callbackData.containsKey('toping')) {
      if (callbackData['toping'].length == 0) {
        callbackData.remove('toping');
      }
    }
    Get.back(result: callbackData);
  }

  void showBottomSheetToping(BuildContext context) {
    showModalBottomSheet(
      barrierColor: Colors.white.withOpacity(0),
      context: context, 
      builder: (context) {
        return SafeArea(
          child: Container(
            decoration: BoxDecoration(
              color: MainColor.white,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(30), topRight:Radius.circular(30)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 3,
                  blurRadius: 8,
                  offset: const Offset(0, 3),
                ),
              ],
            ),
            padding: const EdgeInsets.only(left: 25, right: 25, top: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 120.w),
                  child: const Divider(
                    color: Color.fromARGB(150, 181, 181, 181),
                    thickness: 1.5,
                  ),
                ),
                10.verticalSpace,
                //content
                Text('Pilih Toping',
                  style: Get.textTheme.headlineSmall!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w700
                  ),
                  textAlign: TextAlign.start,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                15.verticalSpace,
                SizedBox(
                  height: 42.h,
                  width: 100.sw,
                  child: Obx(() {
                    if (DetailMenuController.to.toping.isNotEmpty) {
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: DetailMenuController.to.toping.length,
                        itemBuilder: (context, index) {
                          return Obx(() {
                            bool isSelected = false;
                            if (orderData.containsKey('toping')) {
                              orderData['toping'].forEach((element) {
                                if (element['id_detail'] == toping[index]['id_detail']){
                                  isSelected = true;
                                }
                              });
                            }
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: TopingChip(
                                isSelected: isSelected,
                                onTap: () {
                                  tambahAtauHapusToping(toping[index]);
                                },
                                title: '${DetailMenuController.to.toping[index]['keterangan']}',
                              ),
                            );
                          });
                        },
                      );
                    } else {
                      return Text('Toping tidak tersedia',
                        style: Get.textTheme.headlineSmall!.copyWith(
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                        textAlign: TextAlign.center,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      );
                    }
                  }),
                ),
                20.verticalSpace
                //content
              ],
            ),
          ),
        );
      },
      elevation: 10
    );
  }

  void showBottomSheetLevel(BuildContext context) {
    showModalBottomSheet(
      barrierColor: Colors.white.withOpacity(0),
      context: context, 
      builder: (context) {
        return SafeArea(
          child: Container(
            decoration: BoxDecoration(
              color: MainColor.white,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(30), topRight:Radius.circular(30)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 3,
                  blurRadius: 8,
                  offset: const Offset(0, 3),
                ),
              ],
            ),
            padding: const EdgeInsets.only(left: 25, right: 25, top: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 120.w),
                  child: const Divider(
                    color: Color.fromARGB(150, 181, 181, 181),
                    thickness: 1.5,
                  ),
                ),
                10.verticalSpace,
                //content
                Text('Pilih Level',
                  style: Get.textTheme.headlineSmall!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w700
                  ),
                  textAlign: TextAlign.start,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                15.verticalSpace,
                SizedBox(
                  height: 42.h,
                  width: 100.sw,
                  child: Obx(() {
                    if (DetailMenuController.to.level.isNotEmpty) {
                      return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: DetailMenuController.to.level.length,
                        itemBuilder: (context, index) {
                          return Obx(() {
                            bool isSelected = false;
                            if (orderData.containsKey('level')) {
                              if (orderData['level']['id_detail'] == DetailMenuController.to.level[index]['id_detail']) {
                                isSelected = true;
                              }
                            }
                            return Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: TopingChip(
                                isSelected: isSelected,
                                onTap: () {
                                  tambahAtauHapusLevel(DetailMenuController.to.level[index]);
                                },
                                title: '${DetailMenuController.to.level[index]['keterangan']}',
                              ),
                            );
                          });
                        },
                      );
                    } else {
                      return Text('Toping tidak tersedia',
                        style: Get.textTheme.headlineSmall!.copyWith(
                          fontSize: 15,
                          fontWeight: FontWeight.w500
                        ),
                        textAlign: TextAlign.center,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      );
                    }
                  }),
                ),
                20.verticalSpace
                //content
              ],
            ),
          ),
        );
      },
      elevation: 10
    );
  }

  void showBottomSheetCatatan(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled: true,
      useSafeArea: true,
      barrierColor: Colors.white.withOpacity(0),
      context: context, 
      builder: (context) {
        return SafeArea(
          maintainBottomViewPadding: true,
          child: Container(
            decoration: BoxDecoration(
              color: MainColor.white,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(30), topRight:Radius.circular(30)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 3,
                  blurRadius: 8,
                  offset: const Offset(0, 3),
                ),
              ],
            ),
            padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 120.w),
                  child: const Divider(
                    color: Color.fromARGB(150, 181, 181, 181),
                    thickness: 1.5,
                  ),
                ),
                10.verticalSpace,
                //content
                Text('Buat Catatan',
                  style: Get.textTheme.headlineSmall!.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w700
                  ),
                  textAlign: TextAlign.start,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                10.verticalSpace,
                Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: catatanController,
                        autofocus: true,
                      )),
                    10.horizontalSpace,
                    Container(
                      width: 40,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: MainColor.primary,
                      ),
                      child: IconButton(
                        iconSize: 25,
                        color: MainColor.white,
                        onPressed: () {
                          updateCatatan();
                          Get.back();
                        }, 
                        icon: const Icon(BootstrapIcons.check)
                      ),
                    )
                  ],
                ),
                20.verticalSpace
              ],
            ),
          ),
        );
      },
      elevation: 10
    );
  }

  void showBottomTest(BuildContext context) {
    showModalBottomSheet<void>(
      enableDrag: true,
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: TextField()
        );
      },
    );
  }
  /// mengupdate catatan
  void updateCatatan() {
    if (catatanController.text.isNotEmpty) {
      orderData['catatan'] = catatanController.text;
    } else {
      orderData.remove('catatan');
    }
  }

  /// menambah jumlah item
  void tambahJumlah() {
    orderData['jumlah']++;
    orderData.refresh();
  }

  /// mengurangi jumlah item
  void kurangJumlah() {
    if (orderData['jumlah'] >= 1) {
      orderData['jumlah']--;
    }
    orderData.refresh();
  }

  /// Menambahkan atau menghapus toping
  /// 
  /// Kalau (Map<String, dynamic> data) sudah terdapat di orderData['toping'] maka dia akan menghapus data tersebut. jika masih belum ada maka dia akan menambahkannya
  void tambahAtauHapusToping(Map<String, dynamic> data) {
    if (orderData.containsKey('toping')) {
      var indexData = orderData['toping'].indexWhere((e) {
        return e['id_detail'] == data['id_detail'];
      });
      if (indexData == -1) {
        orderData['toping'].add(data);
      } else {
        orderData['toping'].removeAt(indexData);
      }
    } else {
      orderData['toping'] = [
        data
      ];
    }
    orderData.refresh();
  }

  /// Menambahkan atau menghapus level
  /// 
  /// Kalau (Map<String, dynamic> data) sudah terdapat di orderData maka dia akan menghapus data tersebut. jika masih belum ada maka dia akan menambahkannya
  void tambahAtauHapusLevel(Map<String, dynamic> data) {
    if (orderData.containsKey('level')) {
      if (orderData['level']['id_detail'] == data['id_detail']) {
        orderData.remove('level');
      } else {
        orderData['level'] = data;
      }
    } else {
      orderData['level'] = data;
    }
    orderData.refresh();
  }
}