import 'package:trainee/utils/services/http_service.dart';

class HttpServiceRepository{
  HttpServiceRepository();

  /// ('/auth/login')
  Future<dynamic> signIn(Map<String, dynamic>? body) async {
    final service = await HttpService.dioCall().post(
      '/auth/login',
      data: body
    );
    return service;
  }

}