import 'dart:developer';

import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:trainee/modules/features/detail_promo/repositories/detail_promo_repository.dart';

class DetailPromoController extends GetxController{
  static DetailPromoController get to => Get.find<DetailPromoController>();
  final RxInt idPromo = 0.obs;
  final RxString nama = ''.obs;
  final RxString diskon = ''.obs;
  final RxString syarat = ''.obs;

  @override
  void onInit() async {
    super.onInit();
    idPromo.value = Get.arguments;
    var token = await Hive.box('venturo').get('token');
    log(token.toString());
    var detailPromo = await DetailPromoRepository().getDetailPromo(id: idPromo.value, token: token);
    log(detailPromo.toString());
    nama.value = detailPromo['nama'];
    diskon.value = detailPromo['diskon'].toString();
    syarat.value = detailPromo['syarat_ketentuan'];
  }
}