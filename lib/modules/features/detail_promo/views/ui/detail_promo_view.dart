import 'package:bootstrap_icons/bootstrap_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/modules/features/detail_promo/controllers/detail_promo_controller.dart';

import '../../../beranda/views/components/promo_card.dart';

class DetailPromoView extends StatelessWidget{
  const DetailPromoView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10,
        backgroundColor: MainColor.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20.0),
            bottomRight: Radius.circular(20.0),
          ),
        ),
        leading: IconButton(
          iconSize: 23,
          onPressed: () {
            Get.back();
          }, 
          icon: const Icon(Icons.arrow_back_ios_new,
            color: MainColor.black,
          )
        ),
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Icon(
              BootstrapIcons.ticket_detailed,
              color: MainColor.primary,
            ),
            10.horizontalSpace,
            Text('Detail Menu', 
              style: Get.textTheme.titleMedium!.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: MainColor.black
              ),
            ),
          ],
        ),
        centerTitle: true,
      ),
      body: SizedBox(
        width: 100.sw,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: MainColor.background,
              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
              width: 100.sw,
              height: 280.h,
              child: Obx(
                () => PromoCard(
                  enableShadow: false,
                  promoName: 'Promo ${DetailPromoController.to.nama}',
                  discountNominal: '${DetailPromoController.to.diskon}',
                  thumbnailUrl:
                    "https://javacode.landa.id/img/promo/gambar_62661b52223ff.png",
                )
              )
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(right: 25, left: 25, top: 27),
                width: 100.sw,
                decoration: const BoxDecoration(
                  color: MainColor.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(100), topRight: Radius.circular(100))
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        width: 100.sw,
                        child: Text(
                          'Nama Promo',
                          style: Theme.of(context).textTheme.labelLarge!.copyWith(
                            fontSize: 18
                          ),
                        ),
                      ),
                      10.verticalSpace,
                      SizedBox(
                        width: 100.sw,
                        child: Obx(
                          () => Text(
                            DetailPromoController.to.nama.value,
                            style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color: MainColor.primary
                            ),
                          ),
                        )
                      ),
                      5.verticalSpace,
                      const Divider(
                        thickness: 1,
                      ),
                      5.verticalSpace,
                      SizedBox(
                        width: 100.sw,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Icon(
                              BootstrapIcons.card_list,
                              size: 25,
                            ),
                            10.horizontalSpace,
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    'Syarat Dan Ketentuan',
                                    style: Theme.of(context).textTheme.labelLarge!.copyWith(
                                      fontSize: 16
                                    ),
                                  ),
                                  20.verticalSpace,
                                  Obx(
                                    () => Text(
                                    DetailPromoController.to.syarat.value,
                                    style: Theme.of(context).textTheme.labelLarge!.copyWith(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    textAlign: TextAlign.justify,
                                  )
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}