import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:trainee/utils/services/http_service.dart';

class DetailPromoRepository {
  Future<Map<String, dynamic>> getDetailPromo({required id, required String token}) async {
    try {
      var result = await HttpService.dioCall(token: token).get('/promo/detail/$id');
      if (result.statusCode == 200) {
        final Map<String, dynamic> dataResult = result.data['data'];
        return dataResult;
      } else {
        return {};
      }
    } catch(e, stack) {
      await Sentry.captureException(
        e,
        stackTrace: stack
      );
      return {};
    }
  }
}