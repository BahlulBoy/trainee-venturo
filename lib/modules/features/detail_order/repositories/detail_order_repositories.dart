import 'package:hive_flutter/hive_flutter.dart';
import 'package:trainee/modules/features/detail_order/modules/detail_order_model.dart';
import 'package:trainee/utils/services/http_service.dart';

class DetailOrderRepository{
  Future<DetailData> getDetailOrder(int id) async {
    var token = await Hive.box('venturo').get('token');
    var result = await HttpService.dioCall(token: token).get('/order/detail/$id');
    if (result.statusCode == 200) {
      return DetailData.fromJson(result.data['data']);
    } else {
      return DetailData();
    }
  }

  Future<Map<String, dynamic>> cancelOrder(int id) async {
    var token = await Hive.box('venturo').get('token');
    var result = await HttpService.dioCall(token: token).post('/order/batal/$id');
    if (result.statusCode == 200) {
      return result.data['data'];
    } else {
      return {};
    }
  }
}