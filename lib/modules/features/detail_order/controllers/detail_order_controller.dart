
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:trainee/modules/features/detail_order/modules/detail_order_model.dart';
import 'package:trainee/modules/features/detail_order/repositories/detail_order_repositories.dart';

import '../views/components/order_cancel_dialog.dart';

class DetailOrderController extends GetxController{
  static DetailOrderController get to => Get.find<DetailOrderController>();
  late final DetailOrderRepository _orderDetailRepository;

  // order data
  RxString orderDetailState = 'loading'.obs;
  Rxn<DetailData> order = Rxn();
  Timer? timer;

  @override
  void onInit() async {
    super.onInit();
    _orderDetailRepository = DetailOrderRepository();
    final orderId = int.parse(Get.parameters['orderId'] as String);

    await getOrderDetail(orderId).then((value) {
      timer = Timer.periodic(
        const Duration(seconds: 10),
        (_) => getOrderDetail(orderId, isPeriodic: true),
      );
    });
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  Future<void> getOrderDetail(int orderId, {bool isPeriodic = false}) async {
    if (!isPeriodic) {
      orderDetailState('loading');
    }
    try {
      DetailData result = await _orderDetailRepository.getDetailOrder(orderId);
      orderDetailState('success');
      order(result);
    } catch (exception, stacktrace) {
      await Sentry.captureException(
        exception,
        stackTrace: stacktrace,
      );
      orderDetailState('error');
    }
  }

  void showCancelDialog() async {
    var result = await Get.defaultDialog(
      title: '',
      titleStyle: const TextStyle(fontSize: 0),
      content: const OrderCancel()
    );
    
    if (result != null) {
      EasyLoading.show(
        status: 'Sedang Diproses...',
        maskType: EasyLoadingMaskType.black,
        dismissOnTap: false,
      );
      cancelOrder(id: order.value!.order!.idOrder!);
    }
  }

  Future<void> cancelOrder({required int id}) async {
    var result = await DetailOrderRepository().cancelOrder(id);
    if (result.containsKey('id_order')) {
      EasyLoading.dismiss();
      Get.back(result: 'hapus');
    }
  }

  List<Detail> get foodItems =>
      order.value?.detail?.where((element) => element.kategori == 'makanan').toList() ?? [];

  List<Detail> get drinkItems =>
      order.value?.detail?.where((element) => element.kategori == 'minuman').toList() ?? [];

  List<Detail> get snackItems =>
      order.value?.detail?.where((element) => element.kategori == 'snack').toList() ?? [];
}