import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:trainee/modules/features/checkout/views/components/rounded_app_bar.dart';

import '../components/order_list_sliver.dart';
import '../../../beranda/views/components/section_header.dart';
import '../../../checkout/views/components/tile_option.dart';
import '../../controllers/detail_order_controller.dart';
import '../components/order_tracker.dart';

class DetailOrderView extends StatelessWidget{
  const DetailOrderView({super.key});
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  
  @override
  Widget build(BuildContext context) {
    analytics.setCurrentScreen(
      screenName: 'Detail Order Screen',
      screenClassOverride: 'Trainee',
    );

    return Scaffold(
      appBar: RoundedAppBar(
        title: 'Order',
        icon: Icons.shopping_bag_outlined,
        actions: [
          Obx(
            () => Conditional.single(
              context: context,
              conditionBuilder: (context) =>
                  DetailOrderController.to.order.value?.order!.status == 0,
              widgetBuilder: (context) => Padding(
                padding: EdgeInsets.symmetric(vertical: 16.h, horizontal: 10.w),
                child: TextButton(
                  onPressed: () {
                    DetailOrderController.to.showCancelDialog();
                  },
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    visualDensity: VisualDensity.compact,
                  ),
                  child: Text(
                    'Cancel'.tr,
                    style: Get.textTheme.labelLarge
                        ?.copyWith(color: const Color(0xFFD81D1D)),
                  ),
                ),
              ),
              fallbackBuilder: (context) => const SizedBox(),
            ),
          ),
        ],
      ),
      body: Obx(
        () => ConditionalSwitch.single(
          context: context, 
          valueBuilder: (context) =>
              DetailOrderController.to.orderDetailState.value, 
          caseBuilders: {}, 
          fallbackBuilder: (context) => CustomScrollView(
            physics: const ClampingScrollPhysics(),
            slivers: [
              if (DetailOrderController.to.foodItems.isNotEmpty) ...[
                SliverToBoxAdapter(child: 28.verticalSpace),
                SliverToBoxAdapter(
                  child: SectionHeader(
                    icon: Icons.fastfood,
                    title: 'Food',
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: OrderListSliver(
                    orders: DetailOrderController.to.foodItems,
                  ),
                )
              ],
              if (DetailOrderController.to.drinkItems.isNotEmpty) ...[
                SliverToBoxAdapter(child: 17.verticalSpace),
                SliverToBoxAdapter(
                  child: SectionHeader(
                    icon: Icons.local_drink,
                    title: 'Drink',
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: Obx(() => OrderListSliver(
                      orders: DetailOrderController.to.drinkItems,
                    )
                  ),
                )
              ],
              if (DetailOrderController.to.snackItems.isNotEmpty) ...[
                SliverToBoxAdapter(child: 17.verticalSpace),
                SliverToBoxAdapter(
                  child: SectionHeader(
                    icon: Icons.food_bank,
                    title: 'Snack',
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SliverPadding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 25.w, vertical: 8.h),
                  sliver: Obx(() => OrderListSliver(
                      orders: DetailOrderController.to.snackItems,
                    )
                  ),
                )
              ],
            ],
          ),
        )
      ),
      bottomNavigationBar: Obx(
        () => Conditional.single(
          context: context, 
          conditionBuilder: (context) =>
              DetailOrderController.to.orderDetailState.value == 'success', 
          widgetBuilder: (context) => Container(
            decoration: BoxDecoration(
              color: Colors.grey[100],
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(30.r),
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 25.h, horizontal: 22.w),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Total order tile
                      TileOption(
                        title: 'Total orders',
                        subtitle:
                            '(${DetailOrderController.to.order.value?.detail!.length} Menu):',
                        message:
                            'Rp ${DetailOrderController.to.order.value?.order!.totalBayar ?? '0'}',
                        titleStyle: Get.textTheme.titleMedium,
                        messageStyle: Get.textTheme.titleMedium!.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w600),
                      ),
                      // Divider(color: Colors.black45, height: 2.h),

                      // Discount tile
                      Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            DetailOrderController.to.order.value?.order!.diskon ==
                                1 &&
                            DetailOrderController.to.order.value!.order!.potongan! >
                                0,
                        widgetBuilder: (context) => TileOption(
                          icon: Icons.discount_outlined,
                          iconSize: 24.r,
                          title: 'Discount',
                          message:
                              'Rp ${DetailOrderController.to.order.value?.order!.potongan ?? '0'}',
                          titleStyle: Get.textTheme.titleMedium,
                          messageStyle: Get.textTheme.titleMedium?.copyWith(
                              color: const Color(0xFFD81D1D),
                              fontWeight: FontWeight.w600),
                        ),
                        fallbackBuilder: (context) => const SizedBox(),
                      ),
                      Divider(color: Colors.black54, height: 2.h),

                      // Vouchers tile
                      Conditional.single(
                        context: context,
                        conditionBuilder: (context) =>
                            DetailOrderController
                                .to.order.value?.order!.idVoucher !=
                            0,
                        widgetBuilder: (context) => TileOption(
                          icon: Icons.discount,
                          iconSize: 24.r,
                          title: 'voucher'.tr,
                          message:
                              'Rp ${DetailOrderController.to.order.value?.order!.potongan ?? '0'}',
                          messageSubtitle: DetailOrderController
                              .to.order.value?.order!.namaVoucher,
                          titleStyle: Get.textTheme.titleMedium,
                          messageStyle: Get.textTheme.titleMedium?.copyWith(
                              color: const Color(0xFFD81D1D),
                              fontWeight: FontWeight.w600),
                        ),

                        fallbackBuilder: (context) => const SizedBox(),
                      ),
                      if (DetailOrderController.to.order.value?.order!.idVoucher != 0)
                        Divider(color: Colors.black54, height: 2.h),

                      // Payment options tile
                      TileOption(
                        icon: Icons.payment_outlined,
                        iconSize: 24.r,
                        title: 'Payment'.tr,
                        message: 'Pay Later',
                        titleStyle: Get.textTheme.titleMedium,
                        messageStyle: Get.textTheme.titleMedium,
                      ),

                      Divider(color: Colors.black54, height: 2.h),

                      // total payment
                      TileOption(
                        iconSize: 24.r,
                        title: 'Total payment'.tr,
                        message:
                            'Rp ${DetailOrderController.to.order.value?.order!.totalBayar ?? '0'}',
                        titleStyle: Get.textTheme.titleMedium,
                        messageStyle: Get.textTheme.titleMedium!.copyWith(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w600),
                      ),
                      Divider(color: Colors.black54, height: 2.h),
                      24.verticalSpace,

                      // order status track
                      const OrderTracker(),
                    ],
                  ),
                ),
              ],
            ),
          ), 
          fallbackBuilder: (context) => const SizedBox()
        )
      ),
    );
  }
}