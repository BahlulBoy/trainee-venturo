import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:trainee/modules/features/detail_order/views/components/detail_order_card.dart';

import '../../../beranda/repositories/list_repository.dart';
import '../../modules/detail_order_model.dart';

class OrderListSliver extends StatelessWidget{
  const OrderListSliver({
    super.key,
    required this.orders,
  });
  final List<Detail> orders;
  @override
  Widget build(BuildContext context) {
    return SliverFixedExtentList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          var item = orders[index];
          for (var element in ListRepository().imageUrlError) {
            if (element == item.foto) {
              item.foto = null;
            }
          }
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 8.5.h),
            child: DetailOrderCard(
              item,
            ),
          );
        },
        childCount: orders.length,
      ),
      itemExtent: 112.h,
    );
  }

}