import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/themes/main_color.dart';
import 'package:trainee/constants/cores/assets/image_constant.dart';

class OrderCancel extends StatelessWidget{
  const OrderCancel({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.r, vertical: 5.r),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                width: 35,
                height: 35,
                decoration: BoxDecoration(
                  color: MainColor.danger,
                  borderRadius: BorderRadius.circular(50)
                ),
                child: IconButton(
                  iconSize: 18,
                  onPressed: () {
                    Get.back();
                  }, 
                  icon: const Icon(Icons.close,
                    color: MainColor.white,
                    weight: 4,
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            width: 80.w,
            child: Image.asset(ImageConstant.trash),
          ),
          15.verticalSpace,
          SizedBox(
            width: double.infinity,
            child: Text('Hapus Item',
              textAlign: TextAlign.center,
              style: Get.textTheme.bodyMedium!.copyWith(
                fontWeight: FontWeight.w600,
                fontSize: 18
              ),
            ),
          ),
          10.verticalSpace,
          SizedBox(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Text('Apakah anda yakin akan menghapus item tersebut',
                textAlign: TextAlign.center,
                style: Get.textTheme.bodyMedium!.copyWith(
                  fontWeight: FontWeight.w400,
                  fontSize: 13,
                  color: Colors.black38
                ),
              ),
            ),
          ),
          10.verticalSpace,
          SizedBox(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 70.0),
              child: ElevatedButton(
                onPressed: () {
                  Get.back(result: true);
                }, 
                style: ElevatedButton.styleFrom(
                  backgroundColor: MainColor.primary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
                child: const Text('Hapus')
              ),
            ),
          ),
          30.verticalSpace
        ]
      ),
    );
  }
}