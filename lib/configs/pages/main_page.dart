import 'package:get/route_manager.dart';
import 'package:trainee/configs/routes/main_route.dart';
import 'package:trainee/modules/features/beranda/binddings/beranda_binding.dart';
import 'package:trainee/modules/features/beranda/views/ui/beranda_view.dart';
import 'package:trainee/modules/features/checkout/bindings/checkout_binding.dart';
import 'package:trainee/modules/features/checkout/views/ui/checkout_view.dart';
import 'package:trainee/modules/features/detail_menu/bindings/detail_menu_binding.dart';
import 'package:trainee/modules/features/detail_menu/views/ui/detail_menu_view.dart';
import 'package:trainee/modules/features/detail_order/views/ui/detail_order_view.dart';
import 'package:trainee/modules/features/detail_promo/bindings/detail_promo_binding.dart';
import 'package:trainee/modules/features/detail_promo/views/ui/detail_promo_view.dart';
import 'package:trainee/modules/features/initial/views/ui/get_location_screen.dart';
import 'package:trainee/modules/features/no_connection/views/ui/no_connection_view.dart';
import 'package:trainee/modules/features/privacy_policy/views/ui/privacy_policy_view.dart';
import 'package:trainee/modules/features/sign_in/bindings/sign_in_binding.dart';
import 'package:trainee/modules/features/sign_in/views/ui/sign_in_view.dart';
import 'package:trainee/modules/features/splash_screen/binddings/splash_screen_binding.dart';
import 'package:trainee/modules/features/splash_screen/views/ui/splash_screen_view.dart';
import 'package:trainee/modules/features/initial/binddings/initial_binding.dart';
import 'package:trainee/modules/features/vourcher/views/ui/vourcher_view.dart';

import '../../modules/features/detail_order/bindings/detail_order_binding.dart';
import '../../modules/features/vourcher/binddings/vourcher_binding.dart';

abstract class MainPage {
  static final main = [
    /// Setup
    GetPage(
      name: MainRoute.beranda,
      page: () => const Beranda(),
      binding: BerandaBinding()
    ),
    GetPage(
      name: MainRoute.splash, 
      page: () => const SplashScreen(),
      binding: SplashScreenBinding()
    ),
    GetPage(
      name: MainRoute.noConnection, 
      page: () => const NoConnection()
    ),
    GetPage(
      name: MainRoute.signIn, 
      page: () =>const SignIn(),
      binding: SignInBinding()
    ),
    GetPage(
      name: MainRoute.detailMenu, 
      page: () => const DetailMenuView(),
      binding: DetailMenuBinding()
    ),
    GetPage(
      name: MainRoute.initial, 
      page: () => const GetLocationScreen(),
      binding: InitialBinding(),
    ),
    GetPage(
      name: MainRoute.checkout, 
      page: () => const CheckoutView(),
      binding: CheckoutBinding()
    ),
    GetPage(
      name: MainRoute.vourcher, 
      page: () => const VourcherView(),
      binding: VourcherBinding()
    ),
    GetPage(
      name: MainRoute.detailOrder, 
      page: () => const DetailOrderView(),
      binding: DetailOrderBinding()
    ),
    GetPage(
      name: MainRoute.privacyPolicy, 
      page: () => const PrivacyPolicy()
    ),
    GetPage(
      name: MainRoute.detailPromo, 
      page: () => const DetailPromoView(),
      binding: DetailPromoBinding()
    )
  ];
}
