
const Map<String, String> translationsIdId = {
  'Profile': 'Profil',
  'Change': 'Ubah',
  'Change language': 'Ubah bahasa',
  'Change PIN': 'Ubah PIN',
  'Email': 'Alamat Email',
  'Name': 'Nama',
  'Phone number': 'No. telepon',
  'Birth date': 'Tanggal lahir',
  'English': 'Inggris',
  'Indonesia': 'Indonesia',
  ' Your have verified your ID card': ' KTP anda sudah terverifikasi',
  'Verify your ID card now!': 'Verifikasi KTP mu sekarang!',
  'Account info': 'Info Akun',
  'Order' : 'Pesanan',
  'Homepage' : 'Beranda'
};