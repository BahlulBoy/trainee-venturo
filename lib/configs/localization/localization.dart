import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trainee/configs/langs/en_us.dart';
import 'package:trainee/configs/langs/id_id.dart';
import 'package:trainee/constants/cores/assets/image_constant.dart';

class Localization extends Translations{
  // default locale constant
  static const defaultLocale = Locale('id', 'ID');


  // fallback locale constant
  static const fallbackLocale = Locale('en', 'US');

  // supported languages constant
  static const langs = [
    'English',
    'Indonesia',
  ];

  static const flags = [
    ImageConstant.flagEn,
    ImageConstant.flagId
  ];

  static const locales = [
    Locale('en', 'US'),
    Locale('id', 'ID')
  ];

  @override
  Map<String, Map<String, String>> get keys => {
    'en_US' : translationsEnUs,
    'id_ID' : translationsIdId
  };

  static void changeLocale(String lang) async {
    final locale = getLocaleFromLanguage(lang);
    return Get.updateLocale(locale);
  }

  static Locale getLocaleFromLanguage(String lang) {
    for (int i = 0; i < langs.length; i++) {

      if (langs[i].toLowerCase() == lang.toLowerCase()) {
        return locales[i];
      }

    }
    return defaultLocale;
  }

  static Locale get currentLocation {
    return Get.locale ?? fallbackLocale;
  }

  static String get currentLanguage {
    return langs.elementAt(locales.indexOf(currentLocation));
  }
}