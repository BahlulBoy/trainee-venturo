abstract class MainRoute {
  /// Initial
  static const String beranda = '/';
  static const String splash = '/splash';
  static const String noConnection = '/no-connection';
  static const String signIn = '/sign-in';
  static const String initial = '/initial';
  static const String detailMenu = '/detail-menu';
  static const String checkout = '/checkout';
  static const String vourcher = '/vourcher';
  static const String order = '/order';
  static const String detailOrder = '/order/:orderId';
  static const String privacyPolicy = '/privacy_policy';
  static const String detailPromo = '/detail-promo';
}