class ImageConstant {
  ImageConstant._();
  static const String logo = "assets/images/java_code.png";
  static const String imageLocation = "assets/images/location_image.png";
  static const String bgPattern2 = "assets/images/bg_pattern_2.png";
  static const String bgProfile = "assets/images/bg_profile.png";
  static const String icKtp = "assets/svg/ic_ktp.svg";


  static const String flagId = 'assets/svg/flag_id.svg';
  static const String flagEn = 'assets/svg/flag_en.svg';

  static const String trash = 'assets/images/trash_icon.png';
}